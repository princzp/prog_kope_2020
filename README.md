KÓPÉ (Kovács P. László) Programozás szakkör, Tahitótfalu, 2020-2021
===================================================================

**Ingyenes** programozás szakkör indul Tahitótfaluban.

**Kikre számítunk?**
Elsősorban környékbeli, **12 évesnél idősebbekre, gyerekekre és felnőttekre.**
Teljesen kezdőket is várunk, akik még sosem programoztak, de azok sem fognak unatkozni, akik már esetleg igen.
Nem csak jó matekosokat hívunk. Ez nem tehetséggondozó, de azért nem is játszóház.
Az előadások/gyakorlatok egymásra épülnek.

**Hol és mikor?**
A **Tahitótfalui Szent István Király Plébánia Közösségi Termében** (Dózsa Gy. út 17.), szombatonként, délelőtt 10 órától délig. 
Első szakköri alkalom: **2020. október 3., szombat.** Az évad a tervek szerint a tanév végéig, 2 féléven át tart majd.

**Mit fogunk tanulni?**
A szakkör **bevezetés a számítógép-programozás világába, korszerű eszközökkel, interaktívan, játékosan, sok gyakorlással, önálló programozással és sikerélménnyel.** 
Leginkább a Python nyelven keresztül ismerkedünk majd az imperatív programozás fogalmaival, de más nyelvekre és paradigmákra is kitekintéssel: 
érintjük az objektum-orientált és a funkcionális programozást is, a Python mellett megjelenhetnek esetleg még más korszerű nyelvek is. 
Csiszoljuk a problémamegoldó képességünket, gyakoroljuk az algoritmikus gondolkodást, az absztrakciót, az analitikus szemléletet. 
A modern programozó eszköztárának áttekintése, esetleg egy géptermi látogatás zárhatja a szakköri évadot.

**Mit kell hozni?**
**Laptopot (és töltőt, egeret)**, de szülő+gyerek párnak elég összesen egy, ha nincs fejenként egy. **Füzet és írószer kellhet.**

**A szakkörvezető:** **Princz Péter, szoftvermérnök** 
(ELTE TTK programozó matematikus, ELTE IK programtervező informatikus, ELTE TTK matematika-didaktika PhD hallgató).

**Érdeklődni lehet:**
**Szent István Király Plébánia:** 26/385-807 <br>
**Hegedűs István Péter:** +36/30/595-8226 <br>
**Valamint a helyszínen (elavult információ, a járványhelyzet miatt a szakkör online formában folytatódik)**

Hirdetések
==========

1. Okt. 4.: A járványhelyzet miatt a személyes találkozásaink alkalmával fokozottan
   ügyelnünk kell a kézfertőtlenítésre, valamint a szájat és orrot is eltakaró
   maszk viselésére. Az e téren kialakult fegyelmet köszönöm, tartsuk is fenn.
2. Okt 31.: Véleménykérés: mikor legyen a szakkör? Maradjon szombaton délelőtt vagy legyen inkább pénteken délután/koraeste? <br>
   Itt lehet rögzíteni a szavazatokat: [Szakköri időpont szavazás](https://forms.gle/Rg2A5xHgHgTLYXur8)
3. Nov. 1.: A jelenléti ív mostantól elektronikus lesz: [Jelenléti ív](https://docs.google.com/spreadsheets/d/1fEF5ArSr-dVuojV2znfsqWRQbujN93tqgZJIIiU_JMA/edit?usp=sharing)
4. Nov. 4.: A szavazás eredménye: a szakkör maradjon szombaton 10-12 között.
5. Nov. 11.: A járványelyzet miatt a szakkör online formában folytatódik, változatlan időpontban, szombatonként 10.00-12.00 között
6. Nov. 13.: Első online szakkör november 14.-én 10.00-12. között. Konzultációs óra 9.00-10.00 között. <br>
   Google Meet eszközt fogunk használni, a link az első online szakkörre: https://meet.google.com/bha-zura-yss
7. Elkészült a szakköri anyagok videóinak tárhelye: https://www.youtube.com/channel/UCte4ulb6WckIzLH1vB8BLNQ
8. Nov. 27.: Véleménykérés: kvízek bevezetésére lenne-e igény? <br>
   Itt lehet rögzíteni a szavazatokat: [Szavazás kvízjátékokról](https://forms.gle/bkZ49MZ2AXkb4vok6)
9. Jan. 29.: A január 30.-ára tervezett szakkör betegség miatt sajnos elmarad.
10. Feb. 19.: A február 20.-ára tervezett szakkör hétvégi munkahelyi elfoglaltság miatt sajnos elmarad.

Ajánlott irodalom:
==================
* [ThinkCSPy3] Peter Wentworth, Jeffrey Elkner, Allen B. Downey and Chris Meyers: <br>
Hogyan gondolkozz úgy, mint egy informatikus: Tanulás Python 3 segítségével, 3. kiadás. <br>
	* Magyar fordítás online változata: https://mtmi.unideb.hu/pluginfile.php/554/mod_resource/content/1/thinkcspy3.pdf
	* [Magyar fordítás feltöltve erre a tárhelyre](./irodalom/thinkcspy3_hu.pdf)
	* [Angol eredeti feltöltve erre a tárhelyre](./irodalom/thinkcspy3_en.pdf)
* [PythonTurtle] A teknőcgrafika dokumentációja a Python standard könyvtárban:
https://docs.python.org/3.8/library/turtle.html
* [PythonTutorial] A Python saját oktatóanyaga: https://docs.python.org/3.8/tutorial/index.html

# Tematika:
## 1. foglalkozás (október 3.)
* Telepítés, letöltés. Python környezetek, dokumentáció, ajánlott irodalom.
* A Python nyelvről. Python 2.x, 3.x, különbségek.
* Python shell és IDLE IDE. Elindítani, kilépni.
* IDLE képernyő elemek. IDLE konfigurációs beállítások.
* Hello world program. A print utasítás/függvény.
* Többsoros program, több print egymás után. Egy sorban ;-vel elválasztva, vagy több sorban Enter-rel elválasztva. A szekvencia fogalma. Utasítások sorrendjének fontossága.
* Program mentése IDLE editorból .py forrásfile-ba. Python forrásfile-ok futtatása konzolból, és IDLE-be beolvasva.
* Python konzol, mint kalkulátor. Az _ változó mint részeredmény akkumulátor.
* Irodalom:
    * [ThinkCSPy3] 1. fejezet
* [Szakköri anyag](./foglalkozasok/2020_10_03)

## 2. foglalkozás (október 10.)
* Megjegyzésk írása a programszövegbe. Programrészt kivenni/visszatenni hatékonyan. IDLE-ben Format->Comment out region/Uncomment region.
* A változó fogalma. Változók típusa. Nevezetes Python típusok és konverziós függvények: int, float, str.
* Műveletek változókon. Értékadás, többszörös értékadás, párhuzamos értékadás. Változó értékének felülírása, mint temporális kifejezés.
* Irodalom:
    * [ThinkCSPy3] 2. fejezet
* [Szakköri anyag](./foglalkozasok/2020_10_10)

## 3. foglalkozás (október 17.)
* A type() függvény és néhány alaptípus: egész számok, valós számok, szövegek.
* Típusosság. Dinamikus, gyenge és erős típusosság. A Python, mint erős és dinamikus típusozású nyelv.
* Konverzió típusok között műveletekkel és típuskonverziós függvényekkel: int(), float(), str().
* Gondolkodtató feladat: két változó értékének felcserélése.
    * Első, hibás megközelítés: elveszítjük az értéket, mert felülírjuk.
    * Második megközelítés: segédváltozóval.
    * Ugyanez tükrözve a másik változóra.
    * Harmadik megközelítés: segédváltozó sem feltétlenül kell, mert matematikai megközelítést alkalmazunk.
    * Negyedik megközelítés: Python szintaktikus cukorka párhuzamos értékadással.
* Irodalom:
    * Továbbra is [ThinkCSPy3] 1. és 2. fejezet, nincs új anyagrész
* [Szakköri anyag](./foglalkozasok/2020_10_17)

## 4. foglalkozás (október 24.)
* Elágazások a programban.
* if utasítás egyetlen ággal.
* if...else: elágazás két, egymást kizáró ággal.
* Újabb if az else ágban, ezek mély egymásba ágyazása
* Szintaktikus cukorka: az elif kulcsszó.
* Közös programozás: billentyűzetről bekért szám előjelének vizsgálata.
* Irodalom:
    * [ThinkCSPy3] 5. fejezetben az 5.5 és 5.8 közötti rész.
* [Szakköri anyag](./foglalkozasok/2020_10_24)

## 5. foglalkozás (október 31.)
* Rajzolás teknőcgrafikával.
* Seymour Papert és a Logo nyelv.
* Teknőcgrafika Python-ban.
* Az import kulcsszó.
* A turtle csomag.
* Rajzolás a teknőccel vászonra, egyszerű teknőcgrafikai utasítások.
* Szabályos háromszög, négyszög, és ötszóg rajzolása elemi szakaszokból.
* Minta felismerése a sokszög szakaszokból való megrajzolása során.
* Ciklusok a programban.
* Elöltesztelő while ciklus.
* A while kulcsszó a Python-ban.
* Példák:
    * nullaszor
    * pontosan egyszer
    * valahányszor
    * végtelenszer lefutó ciklusra.
* Végtelen ciklus megállítása billentyűzetről: Ctrl-C.
* A szabályos háromszög, négyszög, és ötszóg újrajzolása immár while ciklussal.
* Minta felismerése ezekben a while ciklusokban is.
* Általános, tetszőleges oldalszámra működő, szabályos sokszöget rajzoló program.
* Irodalom:
    * [ThinkCSPy3] 3. fejezet nagy része, kivéve a for ciklusokat (3.3), valamint a 7.4 szakasz
    * [PythonTurtle]
* [Szakköri anyag](./foglalkozasok/2020_10_31)

## 6. foglalkozás (november 7.)
* Gondolkodtató feladatok while ciklussal kapcsolatban: 3-nál kevesebb oldalú sokszög. Fejben programozás az n=2, 1 és 0 esetekre, összevetés a program valós futásával ezekre az esetekre.
* Közös programozás és önálló programozási gyakorlat.
* Sokszögsorozat rajzolása, a múlt heti példa folytatása.
* Ciklus ciklusba ágyazása.
* Ciklusok és elágazások egymásba ágyazása.
* Python teknőcgrafikai könyvtár áttekintése.
* Irodalom:
    * Továbbra is [ThinkCSPy3] 1..3, 5, és 7. fejezetből ami már szerepelt, nincs új anyagrész
    * [PythonTurtle]
* [Szakköri anyag](./foglalkozasok/2020_11_07)

## 7. foglalkozás (november 14.)
* Megjegyzés a múlt heti programhoz: manipuláció elöltesztelő ciklus végén
* Pass utasítás az üres ágaknak, be nem fejezett összetett utasításoknak.
* Véletlenszámok. Véletlenszám-generálás, dobókocka a nem-determinisztikus programokhoz, játékokhoz. A random csomag és a random.randint() függvény.
* Véletlen bolyongás a turtle.goto()-val.
* Irodalom:
    * [ThinkCSPy3] 12.1 rész
    * [PythonTurtle]
	* Wikipedia cikk a véletlen bolyongásról (angolul) https://en.wikipedia.org/wiki/Random_walk
* [Szakköri anyag](./foglalkozasok/2020_11_14)
* Videó anyagok: https://www.youtube.com/playlist?list=PLsZII0OCGAGcfInbtUrF1E3YWB4nTlbD9

## 8. foglalkozás (november 21.)
* A lista fogalma és listakezelés Python-ban. (1)
	* Lista szintaxisa Python-ban: [elem_0, elem_1, ..., elem_n]
	* Egyszerű bevásárlólista példa.
	* Lista hossza: len(lista).
	* Lista indexelése: balról, jobbról. Lista túlindexelése.
	* Lista adott elemének felülírása.
	* Listaelemek típusa. Lista eleme lehet akár lista is. Listák listája.
	* Listák egymás után fűzése (összeadása).
	* Lista szeletelése: [eleje:vége:lépésköz]. Ezek opcionalitása. Gondolkodtató feladat: lista megfordítśa helyben.
	* Szintaktikus cukorka: adott hosszú lista létrehozása azonos elem ismétlésével: [elem] \* hossz
	* Hozzáfűzés lista végére: lista.append(elem).
* A for ciklus. (1)
	* Összevetése a while ciklussal: mikor melyiket célszerű használni.
	* While ciklus átírása ekvivalens for ciklussá.
* Irodalom: [ThinkCSPy3] 3.3..3.5 részek, valamint a 11. fejezet
* [Szakköri anyag](./foglalkozasok/2020_11_21)
* Videó anyagok: https://www.youtube.com/playlist?list=PLsZII0OCGAGetrsaJB8wrn-n7LVLFcEim

## 9. foglalkozás (november 28.)
* A lista fogalma és listakezelés Python-ban. (2)
	* Üres lista: [].
	* Elem törlése listából: lista.remove(elem).
		* ha csak egyszer szerepel a listában
		* ha többször szerepel a listában
		* ha nem is szerepel a listában
* A for ciklus. (2)
	* A range(start, stop[, step]) beépített típus.
	* Ciklus manipulálása a ciklusmagon belül: break, continue, else:.
* Lista bejárása elemenként.
	* Számlálás nélkül, for ciklussal. (Lehetne még: while ciklussal.)
	* Számlálással, for ciklussal. Az enumerate() függvény.
	* Az enumerate() listává alakítása: list(enumerate(lista))
* Irodalom:
	* [ThinkCSPy3] 3.3..3.5 részek, valamint a 11. fejezet
	* [PythonTutorial] 3.1.3., 5.1
* [Szakköri anyag](./foglalkozasok/2020_11_28)
* Videó anyagok: https://www.youtube.com/playlist?list=PLsZII0OCGAGf-Qtg_xIDXxRs7MB2vxkFJ

## 10. foglalkozás (december 5.)
* Programozási gyakorlat: sokszögrajzolás, a korábbi bolyongás feladat továbbgondolása
	* Bolyongás szimuláció két teknőssel. Ötletek:
		* turtle.color(), nem csak pencolor()
		* turtle.xcor()
		* turtle.ycor()
	* Felismerés: ami a ciklus volt a sokszögsorozat programban az algoritmuson, az a lista az adatszerkezeten
	* Kód ismétléséhez ciklus, adat ismétléséhez lista
	* Bolyongás szimuláció tetszőleges számú teknőssel. Ötletek:
		* ne mind az origóból induljon, hanem véletlen helyekről
		* minden valahányadik lépésben hagyjon nyomot turtle.stamp() használatával
* [Szakköri anyag](./foglalkozasok/2020_12_05)
* Videó anyagok: https://youtube.com/playlist?list=PLsZII0OCGAGfXPCUrokrONKNi07i29Hkc

## December 12. SZÜNET

## 11. foglalkozás (december 19.)
* A függvény fogalma, függvények írásának és meghívásának szintaxisa Python-ban. A def kulcsszó.
* Függvényparaméter. Paraméter átadása. Paraméter fogadása lokális változóban. Paraméter nélküli függvények.
* Függvény visszatérési értéke. A return kulcsszó.
* Gyakorlat:
	* A nagyobb() és abszolut_ertek() függvények megírása közösen.
	* Saját függvények saját modulba szervezése, ennek importálása.
* Elhagyható paraméterek alapértelmezett értéke.
* Nevesített paraméterek a függvény meghívásakor.
* Függvény definíciója meg kell előzze a használatát.
* Függvények kompozíciója. Gyakorlat: abszolut_ertek(nagyob()).
* A névtér fogalmának első bevezetése. Láthatóság, élettartam.
* Refaktorálás. Ismétlődő programrészek felismerése, kiemelése külön függvénybe.
* Gyakorlat: teknőspopuláció véletlen bolyongása: teknősök születése a szimuláció közben
* JÖN Irodalom:
	* [ThinkCSPy3] 4. fejezet, a 12.4, 12.5 és 12.6 részek
	* [PythonTutorial] 4.6, 4.7, 9.2
* [Szakköri anyag](./foglalkozasok/2020_12_19)
* Videó anyagok: https://youtube.com/playlist?list=PLsZII0OCGAGcHndJOo-jBy7_U1CANRQsE

## Háttéranyag: mikrokontroller programozás
* Irodalom:
	* Barátságlámpa hobbiprojekt leírása: https://www.instructables.com/Wifi-Synchronized-Lamps/
	* ESP8266 mikrokontroller bevezető: https://en.wikipedia.org/wiki/ESP8266
* [Szakköri anyag](./foglalkozasok/mikrokontroller_programozas)
	* [Mikrokontroller lábkiosztása](./foglalkozasok/mikrokontroller_programozas/WEMOS-MINI-D1_EN_10037901.jpg) <br>
		Pirossal látszanak a táp lábak, fekete a föld. Ezeket piros és fekete színű vezetékkel látni a videókban a próbapanelen. <br>
		A hullámos vonallal jelölt lábakról lehet led-gyűrűket vezérelni. Ezek közül mi a D2-t választottuk. A vezérlőadatok kábelének színe kék a videókban.
	* [Okostelefon alkalmazás kinézete futás közben](./foglalkozasok/mikrokontroller_programozas/Blynk_app_on_android.jpg) <br>
		Ezeket a gombokat a telefonon nyomkodva a mikrokontroller megfelelő lábait magas (1) vagy alacsony (0) feszültségre lehet állítani.
		A lábra kötött áramkör ennek megfelelően viselkedik.
	* [Okostelefon alkalmazás fejlesztői nézet](./foglalkozasok/mikrokontroller_programozas/Blynk_app_dev_view.jpg) <br>
		Ezekre a gomb azonosítókra lehet hivatkozni az Arduino kódban és a telefonos alkalmazásban. <br>
		Pl. figyeljük meg, a "boldog vagyok" gomb kódja (a neki megfeleltetett változó) a "V4".
	* [Arduino példakód: a mikrokontrolleren lévő beépített LED villogtatása](./foglalkozasok/mikrokontroller_programozas/Blink.ino) <br>
		Ezen jól látszik a mikrokontroller programozási modellje: van két nevezetes függvénye, a setup() és a loop(). <br>
		A setup() egyszer fut le, amikor az USB kábelt bedugják vagy a reset gombot megnyomják és ettől újraindul a chip. <br>
		A loop() fut a "végtelenségig", amíg a tápellátás meg nem szűnik vagy reset gombot nem nyomunk.
	* [Okostelefon alkalmazás egy gombja](./foglalkozasok/mikrokontroller_programozas/happy_button.jpg) <br>
		Itt látszik, a "V4" gomb (változó) értéke 0-ból 1-be vált, amikor a gombot megnyomjuk.
	* [Barátságlámpa testreszabandó kód](./foglalkozasok/mikrokontroller_programozas/baratsag_lampa.ino) <br>
		A 13. sorban a komment is jelzi, (C-ben ez //, nem pedig # mint Python-ban), hogy itt mondjuk meg, a chip melyik lábán füleljen a LED-gyűrű. <br>
		A 89. soron foglalkozik azzal az esettel, amikor a "V4" gombot megnyomjuk. 
		Figyeljük meg, hogy kiválaszt egyet az animációk közül: értéket ad az "animation" változónak. 
		A loop()-ban aztán ezeket az animációkat váltogatja, ha volt gombnyomás. <br>
		Az if-es szerkezet teljesen hasonló a Python-hoz, a szintaxis kicsit más. <br>
		A setup()-ban (216. sor) csak beállítja fehér futófényre a LED-eket és felmegy a wifi-re a besütött paraméterekkel (8.-11. sor). <br>
		A loop()-ban (231. sor) váltogatja az előre beállított futófény animációkat aszerint, hogy melyik gombot nyomtuk meg az okostelefonos alkalmazásban.
* Videó anyagok: https://youtube.com/playlist?list=PLsZII0OCGAGcYQvVvbwQOeia12rCtT8D6

## 12. foglalkozás (január 16.)
* A rendezett n-es adatszerekezet (tuple) fogalma és használata.
	* Tuple (rendezett n-es) fogalma.
	* Példák
		* Több visszatérési érték fgv.ből: tuple!. Példa: divmod().
		* Másik nevezetes példa: enumerate() fgv. a range() használatakor: (index, érték) tuple. (Ld. Nov-28.)
	* Tuple létrehozása
		* Példa n>=2-re: tuple létrehozása. Becsomagolás komponensváltozókból.
		* Kivételes szintaxis: n==0 (üres tuple) és n==1 (1-elemű tuple).
		* Üres tuple létrehozása a tuple() függvénnyel.
		* Nemüres tuple létrehozása a tuple() függvénnyel és szekvenciával.
	* Alapműveletek
		* Tuple indexelése, szeletelése.
		* Tuple kicsomagolása komponensváltozókba.
	* Összetettebb műveletek
		* Tuple átadása függvény paraméternek: dupla gömbölyű zárójel.
		* Két tuple összehasonlítása.
		* Szekvenciák (listák) cippzárazása tuple-be a zip() függvénnyel. Cippzár tuple-listává konvertálása a list() függvénnyel.
* Gyakorlat: teknőspopuláció véletlen bolyongása: teknősök születése és növekedése a szimuláció közben. Életkoruk tárolása rendezett n-esekben.
* Irodalom:
	* [ThinkCSPy3] 9. fejezet
	* [PythonTutorial] 5.3 rész
* [Szakköri anyag](./foglalkozasok/2021_01_16)
* Videó anyagok: https://youtube.com/playlist?list=PLsZII0OCGAGczazDxh4IKAv4G3rm7naJQ

## 13. foglalkozás (január 23.)
* A szótár adatszerkezet (dictionary) fogalma és használata.
* Adatszerkezetek visszatekintés
    * Elemi adatszerkezetek: szám, szöveg, logikai érték: int() és float(), string(), True és False
    * Összetett adatszerkezetek: list (lista), tuple (rendezett n-es)
    * Szekvencia adattípusok: string, list, tuple, range. Mert rákövetkezővel felsorolhatóak.
    * dictionary (szótár)
    * set (halmaz)
    * Ezek fogalmi bevezetése, összevetése, melyik mire való.
    * Szintaxis: melyiket hogyan jelöljük:
        * szerepelt: "string", 'string', """multiline string""", '''multiline string'''
        * szerepelt: lista ::= [e1,e2, ..., en]
        * szerepelt: tuple ::= (c1,c2, ..., cn)
        * ma: dict ::= {k1:v1, k2:v2, ... , kn:vn}
        * esetleg: set ::= {e1,e2,...,en}
* Ma: dictionary (szótár)
    * Szótár létrehozása:
        * értékadással: {k1:v1, k2:v2, ... , kn:vn}
        * üres szótár: {}
        * dict() függvénnyel
    * Alapműveletek:
        * Szótárhoz hozzáírás: szótár["új kulcs"]=érték, azaz [i] indexelés, mint listánál
        * Szótárban felülírás (!): mint a hozzáírás fentebb, de létező kulccsal
        * Kulcshoz tartozó érték olvasása szótárból: szótár[kulcs], és szótár.get("kulcs")
        * Törlés szótárból: del szótár["kulcs"]
    * Hasznos műveletek bejáráshoz
        * Szótár kulcsai, értékei, (k, é) tuple-jai: szótár.keys(), values(), items()
        * Nyomtatáshoz kellhet: list(szótár), sorted(szótár)
        * Kulcs tartalmazás ellenőrzése szótárban: "kulcs" in szótár (vagy szótár.keys())
* Valós példa: JSON és dictionary, postai irányítószámok web service
* Gyakorlat: teknősök véletlen bolyongása a síkon: tuple mellett egy dictionary, a családfa nyilvántartására. (Alkalom lehet a rekurzív függvény bevezetésére)
* Irodalom:
	* [ThinkCSPy3] 20. fejezet
	* [PythonTutorial] 5.5 rész (N.B Most 5.4-et a halmazokkal átléptük)
* [Szakköri anyag](./foglalkozasok/2021_01_23)
* Videó anyagok: https://youtube.com/playlist?list=PLsZII0OCGAGfcs91Bufc5etUOnGYb66Ot

## 14. foglalkozás (február 6.)
* (Január 30. betegség miatt elmaradt.)
* Logikai operátorok Python-ban
    * Ismétlés: feltételes utasítás-végrehajtás, logikai típus, logikai kifejezések, True és False logikai konstansok
    * Állítás (ítélet). Állítás igazsághalmaza. Venn-diagramos ábrázolás.
    * Igazságtáblázatok: és, vagy, kizáró vagy műveletek, valamint logikai tagadásé.
    * Boole-algebra
    * bool() típuskonverziós függvény
    * Állítások és halmazok kapcsolata (jelölésekben is): és == metszet, vagy == unió
* Gyakorlat: teszteléskor program elágazásainak átmeneti terelése Boole-algebrai trükkel: x and False, y or True
* Set (halmaz) adattípus
    * Halmaz létrehozása:
        * értékadással: {e1, e2, ..., e4}, v. ö. dictionary
        * üres halmaz létrehozása: set()
    * Műveletek:
        * Halmazhoz hozzáadás: halmaz.add(elem), halmaz.update([iterálható elemek]). NB lehet lista, de akár halmazba halmaz is. Dict-ből a kulcsokat adja hozzá, az értékeket eldobja.
        * Halmaz mérete: len(h)
        * Halmazhoz tartozás ellenőrzése: elem in halmaz
        * Halmaz bejárása for ciklussal: for elem in halmaz: pass
        * Elem törlése halmazból:
            * halmaz.remove(elem), halmaz.discard(elem), elem = halmaz.pop().
            * NB. remove() felteszi, hogy az elem létezik, és KeyError-t dob, ha az elem nem létezik. A discard() csendben nem csinál semmit, ha az elem nem létezik.
            * NB. A halmaz nem rendezett, ezért pop() nem tudni, melyik elemet adja vissza.
        * Egész halmaz kiürítése: halmaz.clear(). NB. mint változó megmarad, csak üres lesz.
        * Halmaz törlése: del halmaz. NB. mint változó is megszűnik, nem lehet már rá hivatkozni.
* Irodalom:
    * [ThinkCSPy3] 5. fejezet nagy része: 5.1, 5.2, 5.3, 5.4, 5.10 a logikai operátorok témakörhöz
    * [PythonTutorial] 5.4 rész a halmazokhoz
    * Python összes halmazművelete: https://docs.python.org/3/library/stdtypes.html#set
* [Szakköri anyag, program és táblakép](./foglalkozasok/2021_02_06)
* Videó: https://youtube.com/playlist?list=PLsZII0OCGAGeGeH4v4itwQMZA7EixPnsN

## 15. foglalkozás (február 13.)
* Set (halmaz) adattípus, folytatás: halmazműveletek Python-ban
    * Unió: h3 = h1.union(h2). Vagy: h3 = h1 | h2. NB. update() halmazzal ugyanez, és a duplikált elemeket mindkettő eldobja.
    * Metszet: h3 = h1.intersection(h2). Vagy: h3 = h1 & h2.
    * Csak a metszet megtartása, nem kommutatív, h1-et helyben módosítja: h1.intersection_update(h2)
    * Minden elem megtartása, kivéve a metszeté
        * Szimmetrikus differencia: h3 = h1.symmetric_difference(h2). Vagy: h3 = h1 ^ h2
        * Szimmetrikus differencia, nem kommutatív, h1-et helyben módosítja: h1.symmetric_difference_update(h2)
    * Különbség, nem kommutatív: h1-h2 , h2-h1
* Gyakorlat: teknősök véletlen bolyongása a síkon: ki nem kelt tojásokat halmazba tenni, a szimuláció végefelé kikeltetni őket (halmazba elemeket tenni és halmazt elemenként elfogyasztani).
* Irodalom (nincs új):
    * [ThinkCSPy3] 5. fejezet nagy része: 5.1, 5.2, 5.3, 5.4, 5.10 a logikai operátorok témakörhöz
    * [PythonTutorial] 5.4 rész a halmazokhoz
    * Python összes halmazművelete: https://docs.python.org/3/library/stdtypes.html#set
* [Szakköri anyag](./foglalkozasok/2021_02_13)
* Videó: https://youtube.com/playlist?list=PLsZII0OCGAGdj-2yVxMYGJEZ_9-K_-s2b

## 16. foglalkozás (február 27.)
* (Február 20. munkahelyi elfoglaltság miatt elmaradt.)
* Gyakorlat, közös programozás
* Marsraszállás játék teknőcgrafikával, 5. lépésig jutottunk
* Irodalom
    * Holdraszállós játék, félnapos Python workshop jegyzetei: https://gitlab.com/princzp/4hr_python_workshop
* [Szakköri anyag](./foglalkozasok/2021_02_27)
* Videó: https://youtube.com/playlist?list=PLsZII0OCGAGcFsVZs112GhRrCDZ6wcHML

## 17. foglalkozás (március 6.)
* Gyakorlat, közös programozás
    * Marsraszállás játék teknőcgrafikával, 6. lépéstől
    * Általános függvényrajzoló program váza
* A math standard modul szolgáltatásai
* Irodalom
    * [ThinkCSPy3] 12.3 rész a math modulról
    * Python math modul dokumentációja: https://docs.python.org/3.8/library/math.html
    * Python numerikus modulok áttekintése: https://docs.python.org/3.8/library/numeric.html
    * [PythonTutorial] 10.6 rész, nagyon rövid áttekintés a legfontosabb modulokról:\
      https://docs.python.org/3.8/tutorial/stdlib.html#mathematics
* [Szakköri anyag](./foglalkozasok/2021_03_06)
* Videó: https://youtube.com/playlist?list=PLsZII0OCGAGfQHYrDDGt_saLmCCQHOyWc

## 18. foglalkozás (március 13.)
* Gyakorlat, közös programozás
    * Általános függvényrajzoló program befejezése
    * Kapcsolat a ferde hajítással
* Kivételek, azaz futás közbeni hibák (1)
    * Kulcsszavak (1): try: ... except:
    * A nov. 7.-ei sokszögsorozat rajzoló alapján: kivétel és kezelése
    * Nevezetes beépített kivételek a dokumentációból, konzolos példák
        * nullával osztás
        * billentyűzetről jövő megszakítási kérelem (Ctrl-C)
        * adatszerkezetek indexelési kivételei
        * file-okkal kapcsolatos kivételek (megj: még nem tanultunk filekezelést, de belenézünk)
* Irodalom
    * [ThinkCSPy3] 19. fejezet
    * [PythonTutorial] 8. Errors and Exceptions https://docs.python.org/3/tutorial/errors.html
    * Python beépített kivételek https://docs.python.org/3/library/exceptions.html
* [Szakköri anyag](./foglalkozasok/2021_03_13)
* Videó: https://youtube.com/playlist?list=PLsZII0OCGAGexkMYIiNvHOrko0Xvw95lH

## 19. foglalkozás (március 20.)
* Kivételek, azaz futás közbeni hibák (2)
    * Kulcsszavak (2)
        * try: ... except: már szerepelt
        * Kivételkezelésre ajánlott példa: https://docs.python.org/3/tutorial/errors.html#handling-exceptions
        * finally, else: https://docs.python.org/3/tutorial/errors.html#defining-clean-up-actions
    * Számlanyomtatás példa az összes kulcsszóra
* Objektum-elvű programozás, OOP (1)
    * Osztályok, amelyekkel már találkoztunk: type() által visszaadott típusok, Turtle modul teknőce
    * A class kulcsszó.
    * Osztály és objektum fogalma
    * A konstruktor fogalma: az \_\_init\_\_() függvény Python-ban. (Más nyelvekben: destruktor fogalma.)
    * Szemlélet: szenvedő (=imperatív) ==> cselekvő (=OOP). A self és other egyezményes "kulcsszavak".
    * Példa: síkidom osztály, pár síkidom, kerület- és területszámítások (1)
* Irodalom a kivételkezeléshez
    * [ThinkCSPy3] 19. fejezet
    * [PythonTutorial] 8. Errors and Exceptions https://docs.python.org/3/tutorial/errors.html
    * Python beépített kivételek https://docs.python.org/3/library/exceptions.html
* Irodalom az objektum-elvű programozáshoz:
    * [ThinkCSPy3] 15-16., 22-24. fejezetek
    * [PythonTutorial] 9. fejezet 'Classes' általában, különösen '9.3 A First Look at Classes' \
      https://docs.python.org/3.8/tutorial/classes.html#a-first-look-at-classes
    * OOP szócikk a magyar wikiédián: https://hu.wikipedia.org/wiki/Objektumorient%C3%A1lt_programoz%C3%A1s
* [Szakköri anyag](./foglalkozasok/2021_03_20)
* Videó: https://youtube.com/playlist?list=PLsZII0OCGAGeS_kKiHZrumc6jGMSTCEC7

## 20. foglalkozás (március 27.)
* Objektum-elvű programozás, OOP (2)
    * Adat és műveletek egységbe zárása.
    * Összevetés az imperatív programozással, objektum-elvű programozás előnyei programok karbantartásakor.
    * Öröklődés: "az x az egyfajta y" kapcsolat ("is-a relationship").
    * Példa: síkidom osztály, pár síkidom, kerület- és területszámítások (2).
    * A super() beépített függvény: objektum szülőosztályának elérésére.
    * Osztályhierarchia vizsgálata:
        * isinstance(objektum, osztály)
        * issubclass(osztály, szülőosztály) beépített függvényekkel,
        * osztály.\_\_subclasses\_\_() függvénnyel
        * osztály.\_\_mro\_\_ attribútummal (ill. osztály.mro() függvénnyel).
    * (Többszörös öröklődés problémája, feloldása különböző nyelvekben. MRO, method resolution order)
* Irodalom az objektum-elvű programozáshoz:
    * [ThinkCSPy3] 15-16., 22-24. fejezetek
    * [PythonTutorial] 9. fejezet 'Classes' általában, különösen '9.3 A First Look at Classes' \
      https://docs.python.org/3.8/tutorial/classes.html#a-first-look-at-classes
    * OOP szócikk a magyar wikiédián: https://hu.wikipedia.org/wiki/Objektumorient%C3%A1lt_programoz%C3%A1s
* [Szakköri anyag](./foglalkozasok/2021_03_27)
* Videó: https://youtube.com/playlist?list=PLsZII0OCGAGd6K0o-txRkgaKLHcFGX4Qo

## 21. foglalkozás, konzultáció #1  (április 17.)

## 22. foglalkozás, konzultáció #2  (április 24.)

## SZAKKÖR VÉGE

