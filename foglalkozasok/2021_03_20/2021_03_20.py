Python 3.8.5 (default, Jul 20 2020, 19:48:14) 
[GCC 7.5.0] on linux
Type "help", "copyright", "credits" or "license()" for more information.
>>> szam_1 = 42
>>> szam_2 = 42.0
>>> szoveg = "hello"
>>> lista = ["kenyér", "liszt"]
>>> szotar = {}
>>> type(szam_1)
<class 'int'>
>>> type(szam_2)
<class 'float'>
>>> type(szoveg)
<class 'str'>
>>> type(lista)
<class 'list'>
>>> type(szotar)
<class 'dict'>
>>> import turtle
>>> teknos = turtle.Turtle()
>>> type(teknos)
<class 'turtle.Turtle'>
>>> type("hello")
<class 'str'>
>>> szam_3 = 28
>>> type(szam_1), type(szam_3)
(<class 'int'>, <class 'int'>)
>>> 2 + 3
5
>>> "abc" + "def"
'abcdef'
>>> +(2,3)
Traceback (most recent call last):
  File "<pyshell#18>", line 1, in <module>
    +(2,3)
TypeError: bad operand type for unary +: 'tuple'
>>> 2 * 3
6
>>> "aaa " * 3
'aaa aaa aaa '
>>> 
>>> x = 42
>>> h1 = Haromszog()
>>> type(h1)
<class '__main__.Haromszog'>
>>> h1.kerulet()
Síkidom kerülete
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_20/objektum_orientaltsag.py
>>> h1 = Haromszog()
>>> h1.kerulet()
Háromszög kerülete
>>> h1.terulet()
Síkidom területe
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_20/objektum_orientaltsag.py
>>> k1 = Kor()
Traceback (most recent call last):
  File "<pyshell#7>", line 1, in <module>
    k1 = Kor()
TypeError: __init__() missing 1 required positional argument: 'kor_sugara'
>>> k1 = Kor(3)
>>> k2 = Kor(5)
>>> k1.kerulet()
Ennek a körnek 3 a sugara, és 18.84955592153876 a kerülete
>>> k2.kerulet()
Ennek a körnek 5 a sugara, és 31.41592653589793 a kerülete
>>> k1.terulet()
Síkidom területe

