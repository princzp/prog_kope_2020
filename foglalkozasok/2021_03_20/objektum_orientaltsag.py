# 2021.03.20, OOP (1)

# Imperatív példa lett volna
def altalanos_kerulet(sikidom):
    pass
    # if háromszög
    # elif kör
    # ...

"""
               Sikidom

    Haromszog    Teglalap      Kor
"""

import math

class SikIdom():

    def kerulet(self):
        print("Síkidom kerülete")

    def terulet(self):
        print("Síkidom területe")

class Haromszog(SikIdom):

    def kerulet(self):
        print("Háromszög kerülete")

class Teglalap(SikIdom):
    pass

class Kor(SikIdom):
    # Körnek van 1 paramétere, a sugara

    def __init__(self, kor_sugara):
        self.r = kor_sugara

    def kerulet(self):
        print("Ennek a körnek", self.r, "a sugara, és", 2 * self.r * math.pi, "a kerülete")

