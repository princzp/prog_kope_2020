# 2021.03.20: kivételkezelés
# try, except, else, finally
import random

while True:
    try:
        print("try: Számlanyomtatás, vagy sikerül, vagy nem")
        if random.randint(1,6) != 6:
            raise Exception ("try: Nem sikerült a nyomtatás")
        print("try: Sikerült a nyomtatás")
        # ide is jöhetne a break, hiszen az else: nem kötelező

    except Exception as kivetel:
        print("except: Kivételt kaptam el: ", kivetel)
        input("Hárítsd el a nyomtatási hibát!")

    else: # amikor nem váltódott ki kivétel, minden OK. Opcionális ág.
        print("else: Nem volt kivétel, kilépés a végtelen ciklusból")
        break  # ha van else: ág, jöhet a break ide, de mehet a try:-ba is

    finally: # minden esetben végrehajtódik, akár sikerült a try: akár nem, és volt kivétel
        print("finally: minden esetben végrehajtódik")

print("Kész a nyomtatás, mehetünk tovább.")
