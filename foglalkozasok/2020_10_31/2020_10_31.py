# 2020.10.31 foglalkozás
# Rajzolás teknőcgrafikával
# Ciklusok a programban
# Elöltesztelő while ciklus
# =========================

# import kulcsszó: összeválogatott nyelvi funkciókat beemelni (importálni) a programodba

import turtle
# ellenjavallt, de takarékos (utána nem kell turtle.):
# from turtle import *

# Példák még nevezetes csomagokra, amiket importálni kellhet
# import math
# import random

# Rajzolás teknőcgrafikával
# nyitó ceremónia: üres vászon, sebesség, szín, alak
turtle.reset()
# turtle.speed(1)
turtle.shape("turtle")
turtle.pencolor("red")

# Szabályos háromszög rajzolása
# háromszög első oldala és irányba fordulás
turtle.forward(100)
turtle.left(120)

# a háromszög második oldala
turtle.forward(100)
turtle.left(120)

# harmadik oldal és irányba fordulás a legvégén
turtle.forward(100)
turtle.left(120)

# Vegyük észre, hogy háromszor csináltuk ugyanazt, mindhárom oldalra

# Négyzet rajzolása hasonlóan
# 1. oldal
turtle.forward(100)
turtle.left(90)

# 2. oldal
turtle.forward(100)
turtle.left(90)

# 3. oldal
turtle.forward(100)
turtle.left(90)

# 4. oldal
turtle.forward(100)
turtle.left(90)

# Vegyük észre, hogy négyszer csináltuk ugyanazt, mind a négy oldalra

# Ötszög
# 1. oldal
turtle.forward(100)
turtle.left(360/5)

# 2. oldal
turtle.forward(100)
turtle.left(360/5)

# 3. oldal
turtle.forward(100)
turtle.left(360/5)

# 4. oldal
turtle.forward(100)
turtle.left(360/5)

# 5. oldal
turtle.forward(100)
turtle.left(360/5)

# Vegyük észre, hogy ötször csináltuk ugyanazt, mind az öt oldalra...

# Ennyi előkészítés után:
# Ciklusok a programban
# Elöltesztelő while ciklus

# Emlékeztető: egyágú if elágazás volt:

valtozo = 3
if valtozo<5:
    print("teljesült az if, egyszer lefutott a beljebb tabulált programrész")
print("Program folytatása az if után")

# A while ciklus nagyon hasonló, de a beljebb tabulált programrész (ciklusmag, ciklus törzse)
# ciklikusan mindaddig lefut, ameddig a feltétel még igaz.
# A végén (az utolsó tabulált sor után) visszaugrik a while-ra és újból kiértékeli a feltételt.
# Lefuthat:
# - nullaszor, ha a feltétel már kezdetben hamis
# - egyszer, mint egy if, ha a feltétel kezdetben igaz, és azonnal hamissá válik
# - valahányszor
# - végtelenszer, ha a feltétel mindig igaz.
# (Általában, ha elfelejtettük pöckölni a ciklusváltozót. Ctrl-C lelövi, nem kell megijedni.)

# Rajzoljuk újra a 3, 4, 5-szöget, immár while ciklussal!
turtle.reset()
# turtle.speed(1)
turtle.shape("turtle")
turtle.pencolor("blue")

# Háromszög
oldal = 1 
while oldal <= 3:
    turtle.forward(100)
    turtle.left(120)   # 360/3
    oldal = oldal + 1

# Négyszög
oldal = 1 
while oldal <= 4:
    turtle.forward(100)
    turtle.left(90)   # 360/4
    oldal = oldal + 1

# Ötszög
oldal = 1 
while oldal <= 5:
    turtle.forward(100)
    turtle.left(72)   # 360/5
    oldal = oldal + 1

# Vegyük észre, még ezek a rajzoló ciklusok is mennyire hasonlítanak

# Szabályos n-szög rajzolása, ahol n tetszőleges. Ötlet: kérjük be a billentyűzetről.
oldalszam = int(input("Hány oldala legyen a szabályos sokszögnek? "))
# (Oldalhossz hasonlóan, lehetne bekérni billentyűzetről, most legyen egy fix érték, de már változóban.)
oldalhossz = 100

# nyitó ceremónia: üres vászon, sebesség, szín, alak
turtle.reset()
turtle.speed(1)
turtle.shape("turtle")
turtle.pencolor("green")

oldal = 1
while oldal <= oldalszam:
    turtle.forward(oldalhossz)
    turtle.left(360/oldalszam)
    oldal = oldal + 1
print("Kész a sokszöged", oldalszam, "oldallal.")
