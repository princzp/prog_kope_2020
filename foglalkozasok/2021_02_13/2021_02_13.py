Python 3.8.5 (default, Jul 20 2020, 19:48:14) 
[GCC 7.5.0] on linux
Type "help", "copyright", "credits" or "license()" for more information.
>>> halmaz_a = {1,2,3}
>>> halmaz_b = {3,4,5}
>>> unio_halmaz = halmaz_a.union(halmaz_b)
>>> unio_halmaz
{1, 2, 3, 4, 5}
>>> halmaz_b.union(halmaz_a)
{1, 2, 3, 4, 5}
>>> halmaz_a.union(halmaz_b) == halmaz_b.union(halmaz_a)
True
>>> metszet_halmaz = halmaz_a.intersection(halmaz_b)
>>> metszet_halmaz
{3}
>>> halmaz_a, halmaz_b
({1, 2, 3}, {3, 4, 5})
>>> print(halmaz_a, halmaz_b)
{1, 2, 3} {3, 4, 5}
>>> halmaz_a.intersection_update(halmaz_b)
>>> print(halmaz_a, halmaz_b)
{3} {3, 4, 5}
>>> halmaz_a = {1,2,3}
>>> print(halmaz_a, halmaz_b)
{1, 2, 3} {3, 4, 5}
>>> halmaz_a.symmetric_difference(halmaz_b)
{1, 2, 4, 5}
>>> halmaz_a - halmaz_b
{1, 2}
>>> halmaz_b - halmaz_a
{4, 5}
>>> halmaz_b.symmetric_difference(halmaz_a)
{1, 2, 4, 5}
>>> halmaz_a.symmetric_difference_update(halmaz_b)
>>> print(halmaz_a, halmaz_b)
{1, 2, 4, 5} {3, 4, 5}
>>> halmaz_a = {1, 2, 3}
>>> print(halmaz_a, halmaz_b)
{1, 2, 3} {3, 4, 5}
>>> halmaz_a.symmetric_difference_update(halmaz_b)
>>> halmaz_a = {1, 2, 3}
>>> print(halmaz_a, halmaz_b)
{1, 2, 3} {3, 4, 5}
>>> halmaz_a.symmetric_difference(halmaz_b)
{1, 2, 4, 5}
>>> print(halmaz_a, halmaz_b)
{1, 2, 3} {3, 4, 5}
>>> halmaz_a.symmetric_difference_update(halmaz_b)
>>> print(halmaz_a, halmaz_b)
{1, 2, 4, 5} {3, 4, 5}
>>> halmaz_a = {1, 2, 3}
>>> print(halmaz_a, halmaz_b)
{1, 2, 3} {3, 4, 5}
>>> halmaz_b.symmetric_difference_update(halmaz_a)
>>> print(halmaz_a, halmaz_b)
{1, 2, 3} {1, 2, 4, 5}
>>> halmaz_b = {3,4,5}
>>> print(halmaz_a, halmaz_b)
{1, 2, 3} {3, 4, 5}
>>> len(halmaz_a)
3
>>> len(halmaz_a - halmaz_b)
2
>>> 2 in halmaz_a
True
>>> 2 in halmaz_b
False
>>> 3 in halmaz_a
True
>>> 3 in halmaz_b
True
>>> halmaz_a.isdisjoint(halmaz_b)
False
>>> halmaz_a.isdisjoint({4,5,6})
True
>>> {1,2,3}.isdisjoint({3,4,5})
False
>>> {1,2,3}.isdisjoint({4,5,6})
True
>>> 