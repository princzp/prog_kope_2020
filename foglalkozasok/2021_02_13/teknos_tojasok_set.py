# Bolyongás szimuláció teknőcgrafikával.
# A táplálékot kereső állat mozgásának egy lehetséges modellje.
# Bolyongás szimuláció tetszőleges számú teknőssel
# 2021. jan. 16. tuple bevezetése
# 2021. jan. 23. dictionary bevezetése
# 2021. feb. 13. set bevezetése

import turtle
import random

def teknos_szuletik(x, y):
    """Létrehoz egy új teknőst és visszatérési értékként visszaadja.
    A listához nem fűzi hozzá, erről a hívónak kell gondoskodnia.
    Méretét sem állítja be, erről is a hívónak kell gondoskodni.
    Ha x és y koordinátának is nulla értékekkel hívják meg, akkor sorsol az új teknősnek.
    Ellenkező esetben a paraméter x és y koordinátára teszi le.
    """
    uj_teknos = turtle.Turtle()
    uj_teknos.shape("turtle")

    # legyenek sorsoltak a színek
    uj_teknos.pencolor(random.random(), random.random(), random.random())
    uj_teknos.color(random.random(), random.random(), random.random())

    # menjen a helyére (vagy sorsolt, vagy a szülőé)
    uj_teknos.penup()
    
    if x == 0 and y == 0:
        uj_teknos.goto(random.randint(-200, 200), random.randint(-200, 200))
    else:
        uj_teknos.goto(x, y)

    uj_teknos.pendown()

    return uj_teknos

hany_teknos = random.randint(1, 10) # int(input("Hány teknős végezzen bolyongást? "))
print("Kezdődik a szimuláció", hany_teknos, "teknőssel")

# N. B. eddig teknősök egyszerű listája volt.
# Mostantól (teknős, szül. év) rendezett n-esek (n=2), tuple-ok listája
teknos_hadsereg = []

# Jan-23: {szulo_indexe: [gyerekek_listája]}.
# Pl. {22: [42, 56, 78], 42:[99, 101], 156: [300]}
teknos_csaladfa = {}

# Feb-13: {(szülő_1, tojás_1), (szülő_1, tojás_2), ..., (szülő_n, tojás_m)}
# Pl. {(22, "tojás425"), (22, "tojás426")..., ...}
ki_nem_kelt_tojasok = set()

i = 1
while i <= hany_teknos:
    # teknos_hadsereg.append(turtle.Turtle())
    kezdo_teknos = teknos_szuletik(0, 0)

    teknos_tuple = kezdo_teknos, 0
    # teknos_hadsereg.append(kezdo_teknos)
    teknos_hadsereg.append(teknos_tuple)

    i = i + 1

# nyitó ceremónia: üres vászon, sebesség, szín, alak
# turtle.reset()
turtle.Screen()
# turtle.speed(1)

bolyongas_hossza = 500 # 1000  # hányszor pörögjön a ciklus
lepes = 1
while lepes <= bolyongas_hossza:

    for teknos, mikor_szuletett in teknos_hadsereg:
        teknos.goto(teknos.xcor() + random.randint(-10, 10), teknos.ycor() + random.randint(-10, 10))

        if lepes % 100 == 0:
            teknos.stamp()

    # print(lepes, "/", bolyongas_hossza)  # diagnosztikai print a konzol ablakba

    dobokocka_1 = random.randint(1, 6)
    dobokocka_2 = random.randint(1, 6)
    
    if dobokocka_1 == 6 and dobokocka_2 == 6:
        szulo = random.randint(0, len(teknos_hadsereg)-1)

        if lepes - teknos_hadsereg[szulo][1] >= 100:    # tényleg teknős születik
            szulo_x = teknos_hadsereg[szulo][0].xcor()
            szulo_y = teknos_hadsereg[szulo][0].ycor()

            teknos_baba = teknos_szuletik(szulo_x, szulo_y)
            teknos_baba.turtlesize(0.5, 0.5, 0.5)
            teknos_tuple = teknos_baba, lepes
            teknos_hadsereg.append(teknos_tuple)

            print(lepes, "/", bolyongas_hossza, ": Teknős baba született, már", len(teknos_hadsereg), "teknős végez bolyongást.")

            if szulo not in teknos_csaladfa.keys():
                # első teknősbaba megszületése
                teknos_csaladfa[szulo]=[len(teknos_hadsereg)]
            else:
                # 2, ...n. teknősbaba megszületése
                gyerekek = teknos_csaladfa[szulo]
                gyerekek.append(len(teknos_hadsereg))
                teknos_csaladfa[szulo]= gyerekek

            print(lepes, "/", bolyongas_hossza, ": Teknős #", szulo, ".szülő családfája most:", teknos_csaladfa[szulo])

        else:   # sajnos túl fiatal, hiába szerencsés
            print(lepes, "/", bolyongas_hossza, ": Sajnos", teknos_hadsereg[szulo][1], "születésű, most nem kelt ki a tojása")
            tojas_tuple = szulo, "tojás" + str(lepes)
            ki_nem_kelt_tojasok.add(tojas_tuple)
            print(lepes, "/", bolyongas_hossza, ": Ki nem kelt tojások halmaza most:", ki_nem_kelt_tojasok)
    
    lepes = lepes + 1  # nagyon fontos! (Különben végtelen ciklus)

print("Bolyongás kész.")

print("Eddig ki nem kelt tojások kikelése")
print(ki_nem_kelt_tojasok)
for szulo_tojas_tuple in ki_nem_kelt_tojasok:
    print(szulo_tojas_tuple) # [0] = szülő, [1] = "tojásééé"

    if szulo_tojas_tuple[0] not in teknos_csaladfa.keys():
        # nincs gyereke, első tojás kikelése
        teknos_csaladfa[szulo_tojas_tuple[0]]=[szulo_tojas_tuple[1]]
    else:
        # 2, ...n. teknősbaba megszületése után tojás(ok) hozzáfűzése
        gyerekek = teknos_csaladfa[szulo_tojas_tuple[0]]
        gyerekek.append(szulo_tojas_tuple[1])
        teknos_csaladfa[szulo_tojas_tuple[0]]= gyerekek

print("Családfa nyomtatása")
for szulo in sorted(teknos_csaladfa.keys()):
    print("Szülő:", szulo, "gyerekei:", teknos_csaladfa[szulo])

print("Programfutás vége.")
