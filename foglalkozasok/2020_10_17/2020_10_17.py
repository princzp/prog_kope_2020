# 2020.10.17
############

valtozo_1 = 5
valtozo_2 = 5.0
valtozo_3 = "szia"

# type() függvény és néhány alaptípus: int(), float(), str()
print(valtozo_1, valtozo_2, valtozo_3)
print(type(valtozo_1))
print(type(valtozo_2))
print(type(valtozo_3))

# Python dinamikus típusos nyelv: nem kell megmondd a típust, a környezetből kitalálja
z1 = 36  # kitalálja, hogy int()
z2 = "hello"  # kitalálja, hogy string
print(z1, "z1 típusa most:", type(z1))
print(z2, "z2 típusa most:", type(z2))

# Valamint: ugyanabban a változónévbe eltérő típusú érték is betehető, az előző felülírható
z1 = 36.0  # eddig int() volt benne, de már nem
print(z1, "z1 típusa most:", type(z1))

### Erősen típusos nyelvekben, pl. Java-ban, le kell írd az int kulcsszót, és később már nem tehetsz bele mást
##int z1 = 36

# Python típusosságának következménye: változó más típussal is felülírható
v = 5
print(v, type(v))

v = "szia"
print(v, type(v))

szamlalo = 4  # kitalálja, hogy a 4 egész (int)
nevezo = 7  # 7 szintén
hanyados = szamlalo / nevezo  # viszont a két egész (int) hányadosa valós (float)
print("Számláló típusa:", type(szamlalo), "nevező típusa:", type(nevezo), "hányados típusa:", type(hanyados))

# egészek szorzata egész
print(szamlalo, type(szamlalo), szamlalo*5, "egészek szorzata:", type(szamlalo*5))
# egész és valós szorzata valós
print(szamlalo, type(szamlalo), szamlalo*5.0, "egész * valós:", type(szamlalo*5.0))

# trükk: ha nem jut eszedbe a float() típuskonverziós függvény, szorozd meg egy egész nullával (valóssal, nem 1-el)
print(nevezo, type(nevezo), nevezo*1.0, type(nevezo*1.0))

# type() függvény és alaptípusok: int(), float(), str()
egesz = 42
print(egesz, type(egesz))

valos = float(egesz)
print(valos, type(valos))

pi = 3.14
print(pi, type(pi))

valami = int(pi)
print(valami, type(valami))

szoveg_pi = str(pi)
print(szoveg_pi, type(szoveg_pi))

print(pi*2, szoveg_pi*2)

### input() függvény
##nev = input("Szia, hogy hívnak? ")
##print("Szia", nev, ", most már tudom a neved...")
##print(nev, type(nev), int(nev))

# Gondolkodtató feladat: felcserélni két változót
v1 = 42
v2 = 37
print("v1 most:", v1, "v2 most:", v2)

### 1. megközelítés: hibás
##v1 = v2 # v1
##v2 = v1

### 2/a. megközelítés: segédváltozó. Jó!
##seged = v1
##v1 = v2
##v2 = seged

### 2/b ugyanez tükrözve
##seged = v2
##v2 = v1
##v1 = seged

### 3. megközelítés: segédváltozó sem feltétlenül kell!
##v2 = v1 + v2
##v1 = v2 - v1
##v2 = v2 - v1
##
### 4. megközelítés: Python szintaktikus cukorka
### többszörös értékadás: ugyanaz az egy, közös érték több változónak
##v1 = 42
##v2 = 42
##v3 = 42
##v4 = 42
### ez ugyanaz, mint
##v1 = v2 = v3 = v4 = 42
##
### párhuzamos értékadás (ez kell nekünk a gondolkodtató feladathoz)
##v1 = 25
##
##v2 = 3.14
##v3 = "hello"
##
##v1, v2, v3 = 25, 3.14, "hello"

# Tehát a 4. megközelítés
v1 , v2 = v2, v1
print("v1 most:", v1, "v2 most:", v2)
