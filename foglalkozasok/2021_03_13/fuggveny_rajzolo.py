import turtle
import math

turtle.screensize(800,600)
turtle.bgcolor("grey")

rajzolo = turtle.Turtle()
rajzolo.hideturtle()
rajzolo.penup()
rajzolo.shape("turtle")
rajzolo.pen(fillcolor="pink", pencolor="blue")

for x in range(-400, 400):
    y = -4*(x/40)**2 + 200
    print(x, y)

    rajzolo.goto(x, y)
    rajzolo.pendown()
    rajzolo.showturtle()

print("Függvényrajzolás kész.")
