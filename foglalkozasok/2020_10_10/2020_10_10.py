# 2020. okt. 10. foglalkozás jegyzete
#####################################

# Ismétlés a múlt hétről
print(2+3)
print("Helló világ!")
print(2+3, 2-3, 2*3, 2/3)
print(2<3, 2>3, 2<=3, 2>=3)

# Megjegyzés: ide bármit írhatok, a sor végéig el sem olvassa
# Lehet több soros is a megjegyzés
# Állhat a sor legelején, mint fentebb: ekkor az egész sor megjegyzés
# Akkor is, ha egy értelmes programsor!!!
# vagy valahol a programsorban, de onnantól a sor végéig megjegyzés
print("2020. okt. 10", 2+3,2-3,2*3,2/3) # nem szép, de elhagyható a szóköz

# után bármi állhat, akár másik hashmark is
###########################################
# ======== ~~~~~~~ ^^^^^^^^^^
# sd;ljfs;dfj;sdfjs;djs;dfjzl;fghs ;fa eo;a ;;;alsd;lsk

# Format => Comment Out Region
# Format => Uncomment Region

print(2+3) # algebrai kif.
print("2+3") # szöveg
print("2+3 =", 2+3) # szöveg, kifejezés egy sorban

# változó fogalma
x = 42
print("x értéke most:", x)

# hiba: még nem kapott kezdőértéket
y = 5.8 # mégsem hiba, ha van értéke
print(y)

z1 = "arany"
z2 = "alma"

print(z1)
print(z2)

# Műveletek változókon
print("x+y =", x+y)
print("y+x =", y+x)
print("z1 + z2" , z1+z2)
print("z2 + z1" , z2+z1)

# változó növelése (eml: 42 volt a dobozban)
x = x + 1
print("x értéke most:", x)

eletek = 3
print("Még", eletek, "életed van.")
eletek = eletek - 1
print("Még", eletek, "életed van.")

pontszam = 100
print("Most", pontszam, "pontod van.")
# Szintaktikus cukorka (syntax sugar): += művelet
# pontszam = pontszam + 50
pontszam += 50

# Párja a -=:
eletek -= 1
print("Még", eletek, "életed van.")

x = 37
x = 24

print(x)

v = 3.14
print(v)

