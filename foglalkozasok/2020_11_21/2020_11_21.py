# 2020. november 21.
# Listákról ld. a videófelvételt, illetve Python konzolban dolgoztunk, aminek a leirata alább szerepel.

bevasarlo_lista = ["cukor", "liszt", "tej", "tojás"]
print(bevasarlo_lista)
print(len(bevasarlo_lista))
print(bevasarlo_lista[1])   # liszt
print(bevasarlo_lista[0])   # cukor
print(bevasarlo_lista[2])   # tej
print(bevasarlo_lista[3])   # tojás

# Lista túlindexelése

"""
>>> bevasarlo_lista[4]
Traceback (most recent call last):
  File "<pyshell#12>", line 1, in <module>
    bevasarlo_lista[4]
IndexError: list index out of range
"""

# Lista indexelése jobbról

print(bevasarlo_lista[-1])  # tojás
print(bevasarlo_lista[-2])  # tej
print(bevasarlo_lista[-3])  # liszt
print(bevasarlo_lista[-4])  # cukor

# Lista túlindexelése jobbról

"""
>>> bevasarlo_lista[-5]
Traceback (most recent call last):
  File "<pyshell#14>", line 1, in <module>
    bevasarlo_lista[-5]
IndexError: list index out of range
"""
# Listaelem felülírása
print(bevasarlo_lista)  # ['cukor', 'liszt', 'tej', 'tojás']
print(bevasarlo_lista[1])   # 'liszt'

bevasarlo_lista[1] = "vaj"
print(bevasarlo_lista)  # ['cukor', 'vaj', 'tej', 'tojás']


# Lista elemeinek típusa tetszőleges lehet
# A bevásárlólista csak stringekből áll, de ez nem kötelező
lista_egeszek = [3, -5, 4, 42]  # csak egészekből álló lista
lista_lebegopontos = [3.0, -5.0, 3.14, 2.71]    # lebegőpontos számokból álló lista
altalanos_lista = ["alma", 4, 28.7, "körte"]    # össze-vissza elemekből álló lista

# Lista eleme akár lista is lehet
listaban_lista = ["alma", "körte", "dinnye", ["görög, sárga"], "barack", ["sárga", "őszi"]]
print(listaban_lista)
print(len(listaban_lista))      # 6
print(listaban_lista[0])        # 'alma'
print(listaban_lista[1])        # 'körte'
print(listaban_lista[2])        # 'dinnye'
print(listaban_lista[3])        # ['görög, sárga']
print(type(listaban_lista[2]))  # <class 'str'>
print(type(listaban_lista[3]))  # <class 'list'>

# Listák listája
listak_listaja = [ ["alma", "körte", "dinnye"], [1, 2, 3], ["bolt", "zöldséges", "piac"] ]
print(listak_listaja)

# az egyes listák indexelése a listák listájában
print(listak_listaja[0])        # ['alma', 'körte', 'dinnye']
print(listak_listaja[1])        # [1, 2, 3]
print(listak_listaja[2])        # ['bolt', 'zöldséges', 'piac']

# az egyes listák elemeinek indexelése
print(listak_listaja[0][0])     # 'alma'
print(listak_listaja[0][1])     # 'körte'
print(listak_listaja[0][2])     # 'dinnye'

print(listak_listaja[1][0])     # 1
print(listak_listaja[1][1])     # 2
print(listak_listaja[1][2])     # 3

print(listak_listaja[2][0])     # 'bolt'
# ...

# Listák összeadása, szorzása
# emlékeztető: stringek összeadása:

"""
>>> "arany" + "alma"
'aranyalma'
>>> "arany " * 3
'arany arany arany '
"""

print(bevasarlo_lista)  # ['cukor', 'vaj', 'tej', 'tojás']
eszembe_jutott = ["kenyér", "csoki"]

print(bevasarlo_lista + eszembe_jutott)     # ['cukor', 'vaj', 'tej', 'tojás', 'kenyér', 'csoki']
print(len(bevasarlo_lista), len(eszembe_jutott), len(bevasarlo_lista + eszembe_jutott))     # (4, 2, 6)

print(eszembe_jutott * 3)   # ['kenyér', 'csoki', 'kenyér', 'csoki', 'kenyér', 'csoki']

# Nevezetes alkalmazás: egyetlen listaelem szorzása ==> lista azonos kezdőelemekkel való feltöltése
torpedo = ["."] * 10
print(torpedo)      # ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.']

torpedo[5] = "X"
print(torpedo)      # ['.', '.', '.', '.', '.', 'X', '.', '.', '.', '.']

torpedo[3] = "X"
print(torpedo)      # ['.', '.', '.', 'X', '.', 'X', '.', '.', '.', '.']

print([0] * 8)      # [0, 0, 0, 0, 0, 0, 0, 0]

# A for ciklus

# emlékeztető: while ciklus
print("While ciklus demo")
i = 1
while i <= 10:
    print("i értéke most: ", i)
    i = i + 1

print("While ciklus kész")

# A fenti átírva for ciklussal

print("For ciklus demo")
for i in [1,2,3,4,5,6,7,8,9,10]:
    print("i értéke most: ", i)
    # érdekes: ciklusváltozót nem is kell nekem manipulálni!

print("For ciklus kész")

# Lista bejárása while ciklussal: nem elegáns, de működik

lista = [10,20,30,40,50,60,70,80,90,100]
i = 0
while i < len(lista):
    print("i index, i. elem a listában: ", i , lista[i])
    i = i + 1

print("while listabejárás vége")

# Lista bejárása for ciklussal: elegáns, "pitonikus", jól olvasható

bevasarlo_lista = ["tej", "kenyér", "só", "cukor", "margarin", "tejföl", 3.14]

for elem in bevasarlo_lista:
    print(elem)
