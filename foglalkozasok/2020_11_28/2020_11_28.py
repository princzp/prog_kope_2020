# 2020.11.28
# for ciklus, folytatás. A range()

# lista bejárása: nehézkes, ha nem 10, hanem esetleg 1000 lépés kell
print("Első megközelítés: listával 1..10")
for i in [1,2,3,4,5,6,7,8,9,10]:
    print(i)

# range bevezetése: majdnem ugyanaz, mint fent, de nem 1..10, hanem 0..9
# [0,1,2,3,4,5,6,7,8,9]
print("Második megközelítés: range(stop)")
for i in range(10):  
    print(i)

print("Harmadik megközelítés: range(start, stop)")
for i in range(1,11):
    print(i)

print("Pl. kettesével lépni: range(start, stop, step)")
for i in range(10, 100, 2):
    print(i)
    
    # számjegyek összege 6
    if (i // 10) + (i % 10) == 6:
        print("Megvan!")
        break
    
# Lista bejárása for ciklussal
bevasarlo_lista = ["tej", "kenyér", "cukor"]

for mit_vegyek in bevasarlo_lista:  # lehetne lista konstans is: ["alma", "körte"]: 
    print("Ezt kell venned:", mit_vegyek)

# lista bejárása while ciklussal
elem = 0
while elem < len(bevasarlo_lista):
    print(elem+1, bevasarlo_lista[elem])
    elem = elem + 1


# tipikus játék ciklus: látszólag végtelen, és van benne break

i = 1
while True:
    print("Még játékban van")
    i = i + 1

    if i == 13:
        break

# for ciklus számlálással: az enumerate függvény bevezetése

"""
>>> bevasarlo_lista
['tej', 'kenyér', 'cukor']
>>> print(bevasarlo_lista)
['tej', 'kenyér', 'cukor']
>>> print(enumerate(bevasarlo_lista))
<enumerate object at 0x7fb8fb728b80>
>>> print(list(enumerate(bevasarlo_lista)))
[(0, 'tej'), (1, 'kenyér'), (2, 'cukor')]
>>> 
"""

for sorszam, mit_vegyek in enumerate(bevasarlo_lista):  # lehetne lista konstans is: ["alma", "körte"]: 
    print(sorszam+1, ". helyen ezt kell venned:", mit_vegyek)


# lista bejárása while ciklussal
elem = 0
while elem < len(bevasarlo_lista):
    print(elem+1, bevasarlo_lista[elem])
    elem = elem + 1
