# 2020.10.24: elágazások

szam_mint_string = input("Gépelj be egy egész számot! ")
szam = int(szam_mint_string)
# print(szam_mint_string, type(szam_mint_string), szam, type(szam))

### Megjegyzés: lehetne 1 sorban az input() és az int().
##szam = int(input("Gépelj be egy egész számot! "))
##print(szam, type(szam))
### Ekkor input() a belső fgv., ez van az int()-be ágyazva
### Vegyük észre, nincs is szöveges változó (szam_mint_string).
### Ehelyett egyből az integer típusúba (szam) tesszük,
### de nem a nyers (str), hanem a konvertált (int) értéket.

print("szam<0?", szam < 0, "szam>0?", szam > 0, "szam egyenlő nulla?", szam == 0)

# if-nek lehet csak egy ága
if szam == 2:  # if jelentése: ha, amennyiben. Kettőspont jelentése: akkor.
    print("Látom, a kettőt választottad.")
    print("Most az if: ágban vagyunk")

print("Visszatérünk a fő ágba")

# if-nek lehet két, egymást kizáró ága
if szam < 0:  # if jelentése: ha, amennyiben. Kettőspont jelentése: akkor.
    print("Látom, negatív számot írtál be")
    print("Most az if: ágban vagyunk")

else:
    # else jelentése: különben, egyébként.
    # Itt most: szam > 0, vagy szam == 0. (Azaz szam >= 0.)
    # Matek: "nem kisebb", azaz nagyobb vagy egyenlő.
    print("Látom, nemnegatív számot írtál be")
    print("Most az else: ágban vagyunk")

    if szam > 0:
        print("Látom, pozitv számot írtál be")
        print("Most az else: ágon belül az if:-ben vagyunk")
    else:  # itt már csak szam == 0 eset lehet. Nem Python, hanem matek okokból
        print("Látom, nullát írtál be")
        print("Most az else: ág else: ágában vagyunk")

print("Visszatérünk a fő ágba")

# Python szintaktikus cukorka: elif kulcsszó. else és if összevonva

betu = "c"  # angol ábécé kisbetűk, a..z, 26-féle

if betu == "a":
    print("a")
else:  # "a" már nem lehet, csak b..z
    if betu == "b":  # pont "b"
        print("b")
    else:  # "a" és "b" már nem lehet, csak c..z
        if betu == "c":
            print("c")
        else:  # nem "a", "b", és "c", hanem d..z
            if betu == "d":
                print("d")
                # ...
                # print("z") már a 26*4 = 104. oszlopban kezdődne, jó ronda is

# helyette írhatom:
if betu == "a":
    print("a")
elif betu == "b":
    print("b")
elif betu == "c":
    print("c")
elif betu == "d":
    print("d")
    # ...
elif betu == "z":
    print("z")

print("Iménti programunk elif: -fel")
print("szam<0? ", szam < 0, "szam>0?", szam > 0, "szam egyenlő nulla?", szam == 0)
if szam < 0:  # if = ha, amennyiben, := akkor
    print("Látom, negatív számot írtál be")
    print("Most az if: ágban vagyunk")
elif szam > 0:
    print("Látom, pozitv számot írtál be")
    print("Most az első elif: ágban vagyunk")
elif szam == 0:  # itt már csak szam==0 eset lehet
    print("Látom, nullát írtál be")
    print("Most a második elif: ágban vagyunk")

print("Visszatérünk a fő ágba")

