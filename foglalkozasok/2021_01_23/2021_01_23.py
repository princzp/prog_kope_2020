# 2021-JAN-23: Szótár (dictionary adattípus)

"""
>>> 4 < 5
True
>>> "alma"[0]
'a'
>>> "alma"[1]
'l'
>>> tuple_1 = ("Tekla", 5.0)
>>> tuple_1[0]
'Tekla'
>>> tuple_1[1]
5.0
>>> tuple_1[2]
Traceback (most recent call last):
  File "<pyshell#6>", line 1, in <module>
    tuple_1[2]
IndexError: tuple index out of range
>>> szinek_eh = {"red":"piros", "green":"zöld", "blue":"kék"}
>>> type(szinek_eh)
<class 'dict'>
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék'}
>>> v = "alma"
>>> v
'alma'
>>> ures_szotar = {}
>>> ures_szotar
{}
>>> type(ures_szotar)
<class 'dict'>
>>> masik_ures_szotar = dict()
>>> masik_ures_szotar
{}
>>> type(masik_ures_szotar)
<class 'dict'>
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék'}
>>> szinek_eh['green']
'zöld'
>>> szinek_eh['Green']
Traceback (most recent call last):
  File "<pyshell#20>", line 1, in <module>
    szinek_eh['Green']
KeyError: 'Green'
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék'}
>>> szinek_eh["yellow"]="sárga"
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék', 'yellow': 'sárga'}
>>> szinek_eh["yellow"]="citromsárga"
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék', 'yellow': 'citromsárga'}
>>> x = 27
>>> x
27
>>> x = 42
>>> x
42
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék', 'yellow': 'citromsárga'}
>>> szinek_eh["orange"]="narancssárga"
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék', 'yellow': 'citromsárga', 'orange': 'narancssárga'}
>>> szinek_eh["green"]
'zöld'
>>> szinek_eh["Green"]
Traceback (most recent call last):
  File "<pyshell#34>", line 1, in <module>
    szinek_eh["Green"]
KeyError: 'Green'
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék', 'yellow': 'citromsárga', 'orange': 'narancssárga'}
>>> szinek_eh["Green"]
Traceback (most recent call last):
  File "<pyshell#42>", line 1, in <module>
    szinek_eh["Green"]
KeyError: 'Green'
>>> szinek_eh.get("Green")
>>> print(szinek_eh.get("Green"))
None
>>> szinek_eh.get("Green", "nincs ilyen kulcs")
'nincs ilyen kulcs'
>>> szinek_eh.get("Green", -42)
-42
>>> szinek_eh.get("green", "nincs ilyen kulcs")
'zöld'
>>> "green" == "Green"
False
>>> 42 == 42
True
>>> 42 == 41
False
>>> 42 == None
False
>>> None == None
True
>>> [] == None
False
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék', 'yellow': 'citromsárga', 'orange': 'narancssárga'}
>>> szinek_eh["green"]
'zöld'
>>> szinek_eh["Green"]
Traceback (most recent call last):
  File "<pyshell#56>", line 1, in <module>
    szinek_eh["Green"]
KeyError: 'Green'
>>> szinek_eh.get("green")
'zöld'
>>> szinek_eh.get("Green")
>>> print(szinek_eh.get("Green"))
None
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék', 'yellow': 'citromsárga', 'orange': 'narancssárga'}
>>> del szinek_eh["yellow"]
>>> szinek_eh
{'red': 'piros', 'green': 'zöld', 'blue': 'kék', 'orange': 'narancssárga'}
>>> del szinek_eh["yellow"]
Traceback (most recent call last):
  File "<pyshell#63>", line 1, in <module>
    del szinek_eh["yellow"]
KeyError: 'yellow'
>>>
"""

# Szünet
