# 2020.11.07 foglalkozás
# Rajzolás teknőcgrafikával
# Ciklusok egymásba ágyazása
# Maradékos osztás, % műveleti jel
# Elágazások beágyazása ciklusba
# A standard könyvtár Turtle moduljának felfedezése, elemi teknőcvezérlő utasítások

import turtle

# nyitó ceremónia: üres vászon, sebesség, szín, alak
turtle.reset()
turtle.speed(1)
turtle.shape("turtle")
turtle.pencolor("blue")

# A múlt heti általános sokszögrajzoló folytatása: ne csak n-szöget rajzoljon,
# hanem 3..n között egy sokszögsorozatot: háromszög, négyszög, ötszög, ... , n-szög

oldalszam = int(input("Hány oldala legyen az utolsó szabályos sokszögnek a sorozatban? "))

# (Oldalhossz hasonlóan, lehetne bekérni billentyűzetről, most legyen egy fix érték, de már változóban.)
oldalhossz = 100

melyik_sokszog = 3

while melyik_sokszog <= oldalszam:
    print("Rajzolom a sokszöget ", melyik_sokszog, "oldallal.")
    oldal = 1
    while oldal <= melyik_sokszog:
        print("....", oldal, ".oldal rajzolása")
        turtle.forward(oldalhossz)
        turtle.left(360/melyik_sokszog)
        oldal = oldal + 1

    melyik_sokszog = melyik_sokszog + 1
    if melyik_sokszog % 4 == 0:
        turtle.pencolor("red")
        print("A következő sokszög piros lesz.")
    elif melyik_sokszog % 4 == 1:
        turtle.pencolor("green")
        print("A következő sokszög zöld lesz.")
    elif melyik_sokszog % 4 == 2:
        turtle.pencolor("black")
        print("A következő sokszög fekete lesz.")
    elif melyik_sokszog % 4 == 3:
        turtle.pencolor("blue")
        print("A következő sokszög kék lesz.")

print("Kész a sokszöged", oldalszam, "oldallal.")

# A standard könyvtár Turtle moduljának felfedezése, elemi teknőcvezérlő utasítások
# Ez itt alább nem program, hanem a konzolba soronként beírogatott próbálgatások, ahogyan a dokumentációban
# sorbam haladva megnéztük őket, itt:
# https://docs.python.org/3.8/library/turtle.html

##>>> import turtle
##>>> turtle.reset()
##>>> turtle.goto(100,200)
##>>> turtle.circle(25)
##>>> turtle.dot(10)
##>>> turtle.dot(100)
##>>> turtle.home()
##>>> turtle.forward(100)
##>>> turtle.home()
##>>> turtle.pencolor("red")
##>>> turtle.forward(100)
##>>> turtle.pencolor("green")
##>>> turtle.home()
##>>> turtle.reset()
##>>> turtle.penup()
##>>> turtle.goto(100,200)
##>>> turtle.pendown()
##>>> turtle.home()
##>>> turtle.reset()
##>>> turtle.penup()
##>>> turtle.goto(100,200)
##>>> turtle.stamp()
##39
##>>> turtle.penup()
##>>> turtle.home()
##>>> turtle.stamp()
##40
##>>> turtle.clearstamp(39)
##>>> turtle.hideturtle()
##>>> turtle.clearstamp(40)
##>>> turtle.forward(100)
##>>> turtle.pendown()
##>>> turtle.forward(100)
##>>> 
