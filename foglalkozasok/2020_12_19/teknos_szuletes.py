# Bolyongás szimuláció teknőcgrafikával.
# A táplálékot kereső állat mozgásának egy lehetséges modellje.
# Bolyongás szimuláció tetszőleges számú teknőssel
# Függvény bevezetése

import turtle
import random

def teknos_szuletik(x, y):
    """Létrehoz egy új teknőst és visszatérési értékként visszaadja.
    A listához nem fűzi hozzá, erről a hívónak kell gondoskodnia.
    Méretét sem állítja be, erről is a hívónak kell gondoskodni.
    Ha x és y koordinátának is nulla értékekkel hívják meg, akkor sorsol az új teknősnek.
    Ellenkező esetben a paraméter x és y koordinátára teszi le.
    """
    uj_teknos = turtle.Turtle()
    uj_teknos.shape("turtle")

    # legyenek sorsoltak a színek
    uj_teknos.pencolor(random.random(), random.random(), random.random())
    uj_teknos.color(random.random(), random.random(), random.random())

    # menjen a helyére (vagy sorsolt, vagy a szülőé)
    uj_teknos.penup()
    
    if x == 0 and y == 0:
        uj_teknos.goto(random.randint(-200, 200), random.randint(-200, 200))
    else:
        uj_teknos.goto(x, y)

    uj_teknos.pendown()

    return uj_teknos


hany_teknos = random.randint(1, 10) # int(input("Hány teknős végezzen bolyongást? "))
print("Kezdődik a szimuláció", hany_teknos, "teknőssel")

teknos_hadsereg = []

i = 1
while i <= hany_teknos:
    # teknos_hadsereg.append(turtle.Turtle())
    kezdo_teknos = teknos_szuletik(0, 0)
    teknos_hadsereg.append(kezdo_teknos)

    i = i + 1

# nyitó ceremónia: üres vászon, sebesség, szín, alak
# turtle.reset()
turtle.Screen()
# turtle.speed(1)

##for teknos in teknos_hadsereg:
##    teknos.shape("turtle")
##
##    # legyenek sorsoltak a színek
##    teknos.pencolor(random.random(), random.random(), random.random())
##    teknos.color(random.random(), random.random(), random.random())
##
##    # menjen sorsolt helyre
##    teknos.penup()
##    teknos.goto(random.randint(-200, 200), random.randint(-200, 200))
##    teknos.pendown()

bolyongas_hossza = 1000  # hányszor pörögjön a ciklus

# Most már akárhány teknős lehet ==> nem jó ötlet a külön változó mindnek
# Szerencsére: az adott teknős koordinátáit le lehet kérdezni: ld. turtle.xcor(), turtle.ycor()

##teknos_x = 0  # a teknős x és y koordinátája
##teknos_y = 0

lepes = 1
while lepes <= bolyongas_hossza:

    for teknos in teknos_hadsereg:
        teknos.goto(teknos.xcor() + random.randint(-10, 10), teknos.ycor() + random.randint(-10, 10))

        if lepes % 100 == 0:
            teknos.stamp()

    print(lepes, "/", bolyongas_hossza)  # diagnosztikai print a konzol ablakba

    dobokocka_1 = random.randint(1, 6)
    dobokocka_2 = random.randint(1, 6)
    
    if dobokocka_1 == 6 and dobokocka_2 == 6:
        szulo = random.randint(0, len(teknos_hadsereg)-1)
        szulo_x = teknos_hadsereg[szulo].xcor()
        szulo_y = teknos_hadsereg[szulo].ycor()

        teknos_baba = teknos_szuletik(szulo_x, szulo_y)
        teknos_baba.turtlesize(0.5, 0.5, 0.5)
        teknos_hadsereg.append(teknos_baba)
        print("Teknős baba született, már", len(teknos_hadsereg), "teknős végez bolyongást.")
    
    lepes = lepes + 1  # nagyon fontos! (Különben végtelen ciklus)

print("Bolyongás kész.")
