# 2020.12.19 Függvények
# 2 kulcsszó: def, return
# Példa volt: print(), input()

import random
import azenfuggvenyeim

##def nagyobb(a, b):
##    """Visszaadja két szám közül a nagyobbikat."""
##    if a > b:
##        return a
##    elif a == b:
##        return a
##    elif a < b:
##        return b
##
##def negyzet(y):
##    x = y*y
##    return x
##
##def randint():
##    pass

# láthatóság, élettartam: nem is muszáj (x, y) ==> (a, b)-re. Akár lehet ugyanaz a nevük
##x = random.randint(1, 6)
##y = random.randint(1, 6)

a = random.randint(1, 6)
b = random.randint(1, 6)

nagyobbik_szam = azenfuggvenyeim.nagyobb(a, b)
    
print(a, b, "a nagyobb:", nagyobbik_szam)

x = 5
x_negyzete = azenfuggvenyeim.negyzet(x)
print(x, "négyzete =", x_negyzete)
print("x értéke a négyzetre emelés után:", x)

azenfuggvenyeim.randint()

# függvények kompozíciója
# y = f(x)
# z = g(x)
# z = g(f(x))

print("Függvények kompozíciója: ", azenfuggvenyeim.nagyobb(azenfuggvenyeim.negyzet(5), azenfuggvenyeim.negyzet(-5)))
