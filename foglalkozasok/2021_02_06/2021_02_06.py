Python 3.8.5 (default, Jul 20 2020, 19:48:14) 
[GCC 7.5.0] on linux
Type "help", "copyright", "credits" or "license()" for more information.
>>> 5 == 42
False
>>> 5 != 42
True
>>> 5 < 42
True
>>> 5 > 42
False
>>> 5 <= 42
True
>>> 5 >= 42
False
>>> 7 - 2 == 42
False
>>> 7 - 2 == 5
True
>>> 5 == 42
False
>>> not (5 == 42)
True
>>> not True
False
>>> not False
True
>>> not not True
True
>>> not not False
False
>>> 42
42
>>> -1 * 42
-42
>>> -1 * -1 * 42
42
>>> 2 + 3
5
>>> 2 + 3 + 4
9
>>> True and True
True
>>> True or True
True
>>> True != True
False
>>> szam = 42
>>> bool(szam)
True
>>> szam2 = 0
>>> bool(szam2)
False
>>> bool("")
False
>>> bool("aaa")
True
>>> int("42")
42
>>> miez = {}
>>> type(miez)
<class 'dict'>
>>> ures_halmaz = set()
>>> ures_halmaz
set()
>>> miez
{}
>>> lista = ["alma", "körte", "alma"]
>>> lista
['alma', 'körte', 'alma']
>>> h1 = {"alma", "körte"}
>>> h1
{'alma', 'körte'}
>>> type(h1)
<class 'set'>
>>> h2 = {"alma", "körte", "alma"}
>>> h2
{'alma', 'körte'}
>>> len(h2)
2
>>> "alma" == "alma"
True
>>> h2
{'alma', 'körte'}
>>> h2.add("szilva")
>>> h2
{'szilva', 'alma', 'körte'}
>>> h2
{'szilva', 'alma', 'körte'}
>>> h2
{'szilva', 'alma', 'körte'}
>>> h2
{'szilva', 'alma', 'körte'}
>>> h2
{'szilva', 'alma', 'körte'}
>>> h2
{'szilva', 'alma', 'körte'}
>>> h2.update(["barack", "mangó"])
>>> h2
{'körte', 'barack', 'alma', 'szilva', 'mangó'}
>>> len(h2)
5
>>> h3 = {"kiwi", "alma"}
>>> h2.update(h3)
>>> h2
{'körte', 'kiwi', 'barack', 'alma', 'szilva', 'mangó'}
>>> len(h2)
6
>>> "alma" in h2
True
>>> "aranyalma" in h2
False
>>> "aranyalma" not in h2
True
>>> "alma" not in h2
False
>>> not "alma" in h2
False
>>> h2
{'körte', 'kiwi', 'barack', 'alma', 'szilva', 'mangó'}
>>> for gyumolcs in h2:
	print(gyumolcs)

	
körte
kiwi
barack
alma
szilva
mangó
>>> h2
{'körte', 'kiwi', 'barack', 'alma', 'szilva', 'mangó'}
>>> h2.remove("kiwi")
>>> h2
{'körte', 'barack', 'alma', 'szilva', 'mangó'}
>>> len(h2)
5
>>> h2.remove("kiwi")
Traceback (most recent call last):
  File "<pyshell#75>", line 1, in <module>
    h2.remove("kiwi")
KeyError: 'kiwi'
>>> h2.discard("kiwi")
>>> h2
{'körte', 'barack', 'alma', 'szilva', 'mangó'}
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> 
>>> h2
{'körte', 'barack', 'alma', 'szilva', 'mangó'}
>>> gyumolcs = h2.pop()
>>> gyumolcs
'körte'
>>> h2
{'barack', 'alma', 'szilva', 'mangó'}
>>> len(h2)
4
>>> gyumolcs = h2.pop()
>>> gyumolcs
'barack'
>>> h2
{'alma', 'szilva', 'mangó'}
>>> len(h2)
3
>>> gyumolcs = h2.pop()
>>> gyumolcs = h2.pop()
>>> gyumolcs = h2.pop()
>>> gyumolcs = h2.pop()
Traceback (most recent call last):
  File "<pyshell#97>", line 1, in <module>
    gyumolcs = h2.pop()
KeyError: 'pop from an empty set'
>>> h2 = {"körte", "szilva", "mangó", "barack"}
>>> h2
{'szilva', 'barack', 'körte', 'mangó'}
>>> len(h2)
4
>>> len(h2)
4
>>> type(h2)
<class 'set'>
>>> h2.clear()
>>> h2
set()
>>> len(h2)
0
>>> type(h2)
<class 'set'>
>>> del h2
>>> h2
Traceback (most recent call last):
  File "<pyshell#108>", line 1, in <module>
    h2
NameError: name 'h2' is not defined
>>> len(h2)
Traceback (most recent call last):
  File "<pyshell#109>", line 1, in <module>
    len(h2)
NameError: name 'h2' is not defined
>>> type(h2)
Traceback (most recent call last):
  File "<pyshell#110>", line 1, in <module>
    type(h2)
NameError: name 'h2' is not defined
>>> 
