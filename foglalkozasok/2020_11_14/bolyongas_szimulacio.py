# Bolyongás szimuláció teknőcgrafikával.
# A táplálékot kereső állat mozgásának egy lehetséges modellje.

import turtle
import random  # ez az újdonság

# nyitó ceremónia: üres vászon, sebesség, szín, alak
turtle.reset()
# turtle.speed(1)
turtle.shape("turtle")
turtle.pencolor("blue")

bolyongas_hossza = 1000  # hányszor pörögjön a ciklus
teknos_x = 0  # a teknős x és y koordinátája
teknos_y = 0

# Ezek később kellhetnek
szog = 0  # lehet 0, 360 között
irany = 0 # 0 = bal, 1 = jobb

lepes = 1
while lepes <= bolyongas_hossza:
    # Ebben a deszkamodellben a teknős mindig 10 pixelt lép, a 4 égtáj
    # valamelyikének irányába, és közben végig Kelet felé néz.

    merre = random.randint(1,4)

    if merre == 1:  # Kelet: vízszintesen jobbra
        teknos_x = teknos_x + 10
    elif merre == 2:  # Dél: függőegesen le
        teknos_y = teknos_y - 10
    elif merre == 3:  # Nyugat: vízszintesen balra
        teknos_x = teknos_x - 10
    elif merre == 4:  # Észak: függőlegesen fel
        teknos_y = teknos_y + 10

    turtle.goto(teknos_x, teknos_y)  # menj oda
    print(lepes, "/", bolyongas_hossza)  # diagnosztikai print a konzol ablakba
    lepes = lepes + 1  # nagyon fontos! (Különben végtelen ciklus)

print("Bolyongás kész.")
