# 2020.11.14, 7. szakkör
# előző héthez megjegyzés: while ciklus elöltesztelő => a ciklusmag végén
# nem lehetünk benne biztosak, hogy lesz következő iteráció a ciklusban

# pass utasítás
# Meg nem írt kódrész jelölésére, ahol szintaktikailag kötelező valamilyen
# utasításnak állni, de még nem írtuk meg, a programot viszont futtatni 
# szeretnénk, hogy teszteljük

# Nevezetes példa: többágú if, ahol egyszerre egy ágat írunk meg, de tesztelni
# szeretnénk, áganként

# Pl. a múlt heti programhoz hasonló:

melyik_sokszog = 7

if melyik_sokszog % 4 == 0:
    # kitöltött, megírt ág
    turtle.pencolor("red")
    print("A következő sokszög piros lesz.")
elif melyik_sokszog % 4 == 1:
    # még nem írtam meg
    pass
elif melyik_sokszog % 4 == 2:
    pass
elif melyik_sokszog % 4 == 3:
    # Vajon csak komment elég neki? 
    # Sajnos nem, ezért kell a pass, vagy valami ami már program!
    # Ez lehet egy üres print() is, de a pass a legrövidebb és könnyű
    # rákeresni, az elvarratlan szálakat megtalálni.
    pass


# (Pszeudo)véletlen számok használata játékokhoz, szimulációkhoz
# A random csomag importálása (pont úgy, ahogy a turtle-é)
import random

# random.randint(a,b) a <=N <= b véletlen egész sorsoltatása a géppel
veletlen_szam = random.randint(1,6)  # klasszikus dobókocka
print("dobókocka: ", veletlen_szam)

# Bolyongás szimuláció: külön programban, ld. ott.

