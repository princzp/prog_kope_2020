### 2020.11.14., 7. szakkör
### előző héthez megjegyzés
##
### pass utasítás
### Meg nem írt kódrész jelölése
##
##if 5 % 4 == 0:
##    #kitöltött, megírt ág
##    print("Osztható")
##elif 5 % 4 == 1:
##    #Még nem írtam meg
##    #print("Maradék 1")
##    pass
##elif 5 % 4 == 2:
##    pass
##elif 5 % 4 == 3:
##    # Csak komment elég neki? Sajnos nem, ezért kell a pass, vagy valami ami már program!
##    pass
##
##print("Az utolsó ág is kiértékelődött")
##
##import turtle       
##import random
##
##véletlen_szám = random.randint(1,6) # klasszikus dobókocka
##print("dobókocka:", véletlen_szám)


# Bolyongás szimuláció teknőcgrafikával

import turtle
import random

# nyitó ceremónia: üres vászon, sebesség, szín, alak
turtle.reset()
turtle.speed(10)
turtle.shape("turtle")
turtle.pencolor("blue")

bolyongás_hossza = 1000
teknős_x = 0
teknős_y = 0
#teknős_x, y = 0

szög = 0 # lehet 0,360 között
irány = 0 # 0 = bal, 1 = jobb

lépés = 1
while lépés <= bolyongás_hossza:
    # itt kéne folytatni
    # merre = random.randint(1,4)

    teknős_x = teknős_x + random.randint(-10,10)
    teknős_y = teknős_y + random.randint(-10,10) 
    

##    if merre == 1:
##        teknős_x = teknős_x + random.randint(-10,10)
##        teknős_y = teknős_y + random.randint(-10,10) 
    #elif merre == 2:
        #teknős_y = random.randint(0,100)
    #elif merre == 3:
        #teknős_x = teknős_x - 10
    #elif merre == 4:
        #teknős_y = teknős_y + 10

    turtle.goto(teknős_x, teknős_y)
    print(lépés, "/", bolyongás_hossza)
    lépés = lépés + 1
    
print("Bolyongás vége.")
