import turtle
turtle.reset()
turtle.speed(0)

import random

bolyongas_hossz = 500
lepes = 1
szog = random.randint(0,360)
hossz = random.randint(5,20)

while lepes <= bolyongas_hossz:
    merre = random.randint(1,2)
        
    if merre == 1:
        turtle.forward(hossz)
    elif merre == 2:
        turtle.left(szog)
    
    lepes = lepes + 1
    
print("Bolyongás kész")
