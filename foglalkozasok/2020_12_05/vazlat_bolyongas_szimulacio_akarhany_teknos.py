# Bolyongás szimuláció teknőcgrafikával.
# A táplálékot kereső állat mozgásának egy lehetséges modellje.
# Bolyongás szimuláció tetszőleges számú teknőssel

import turtle
import random

hany_teknos = int(input("Hány teknős végezzen bolyongást? "))

teknos_hadsereg = []

i = 1
while i <= hany_teknos:
    teknos_hadsereg.append(turtle.Turtle())
    i = i + 1

# nyitó ceremónia: üres vászon, sebesség, szín, alak
turtle.reset()
# turtle.speed(1)

for teknos in teknos_hadsereg:
    teknos.shape("turtle")

    # legyenek sorsoltak a színek
    teknos.pencolor(random.random(), random.random(), random.random())
    teknos.color(random.random(), random.random(), random.random())

    # menjen sorsolt helyre
    teknos.penup()
    teknos.goto(random.randint(-200, 200), random.randint(-200, 200))
    teknos.pendown()

bolyongas_hossza = 1000  # hányszor pörögjön a ciklus

# Most már akárhány teknős lehet ==> nem jó ötlet a külön változó mindnek
# Szerencsére: az adott teknős koordinátáit le lehet kérdezni: ld. turtle.xcor(), turtle.ycor()

##teknos_x = 0  # a teknős x és y koordinátája
##teknos_y = 0

lepes = 1
while lepes <= bolyongas_hossza:

    for teknos in teknos_hadsereg:
        teknos.goto(teknos.xcor() + random.randint(-10, 10), teknos.ycor() + random.randint(-10, 10))

        if lepes % 100 == 0:
            teknos.stamp()

    print(lepes, "/", bolyongas_hossza)  # diagnosztikai print a konzol ablakba
    lepes = lepes + 1  # nagyon fontos! (Különben végtelen ciklus)

print("Bolyongás kész.")
