# Bolyongás szimuláció teknőcgrafikával.
# A táplálékot kereső állat mozgásának egy lehetséges modellje.
# Bolyongás szimuláció két teknőssel

import turtle
import random  # ez az újdonság

# nyitó ceremónia: üres vászon, sebesség, szín, alak
# turtle.reset()
turtle.Screen()
# turtle.speed(1)

tobias = turtle.Turtle()
tekla = turtle.Turtle()

tobias.shape("turtle")
tobias.pencolor("blue")
tobias.color("blue")

tekla.shape("turtle")
tekla.pencolor("red")
tekla.color("red")

bolyongas_hossza = 1000  # hányszor pörögjön a ciklus
teknos_tobias_x = 0  # a teknős x és y koordinátája
teknos_tobias_y = 0

teknos_tekla_x = 0
teknos_tekla_y = 0

lepes = 1
while lepes <= bolyongas_hossza:
    # Ebben a deszkamodellben a teknős mindig 10 pixelt lép, a 4 égtáj
    # valamelyikének irányába, és közben végig Kelet felé néz.

    merre_tobias = random.randint(1,4)
    merre_tekla = random.randint(1,4)

    if merre_tobias == 1:  # Kelet: vízszintesen jobbra
        teknos_tobias_x = teknos_tobias_x + 10
    elif merre_tobias == 2:  # Dél: függőegesen le
        teknos_tobias_y = teknos_tobias_y - 10
    elif merre_tobias == 3:  # Nyugat: vízszintesen balra
        teknos_tobias_x = teknos_tobias_x - 10
    elif merre_tobias == 4:  # Észak: függőlegesen fel
        teknos_tobias_y = teknos_tobias_y + 10

    if merre_tekla == 1:  # Kelet: vízszintesen jobbra
        teknos_tekla_x = teknos_tekla_x + 10
    elif merre_tekla == 2:  # Dél: függőegesen le
        teknos_tekla_y = teknos_tekla_y - 10
    elif merre_tekla == 3:  # Nyugat: vízszintesen balra
        teknos_tekla_x = teknos_tekla_x - 10
    elif merre_tekla == 4:  # Észak: függőlegesen fel
        teknos_tekla_y = teknos_tekla_y + 10

    tobias.goto(teknos_tobias_x, teknos_tobias_y)  # menj oda
    tekla.goto(teknos_tekla_x, teknos_tekla_y)  # menj oda

    print(lepes, "/", bolyongas_hossza)  # diagnosztikai print a konzol ablakba
    lepes = lepes + 1  # nagyon fontos! (Különben végtelen ciklus)

print("Bolyongás kész.")
