import turtle
import random
import math

turtle.screensize(800,600)
turtle.bgcolor("grey")

urhajo = turtle.Turtle()
urhajo.hideturtle()
urhajo.penup()
urhajo.shape("circle")
urhajo.pen(fillcolor="pink", pencolor="blue")

# Az űrhajó mozgását leíró változók
urhajo_x = random.randint( int(-1* (turtle.window_width() /2)) , int(-3/4*(turtle.window_width() /2)) )  # random.randint(-400,-300) 
urhajo_y = random.randint( int(3/4*(turtle.window_height()/2)) , int(   1*(turtle.window_height()/2)) )  # random.randint(0.75*300, 300)  
urhajo_sebesseg = 6     # tapasztalati érték, játssz vele!

# A ferde hajítás paraméterei
g = 3.711       # gravitációs állandó. A Földön: 9.807 m/s², a Holdon: 1.622 m/s², a Marson: 3.711 m/s², A Jupiteren: 24.79 m/s². Tapasztalati érték, játssz vele!
szog_fok = 30   # légkörbe belépés szöge, tapasztalati érték, játssz vele!
t = 0           # eltelt (relatív) idő

urhajo.goto(urhajo_x, urhajo_y)
urhajo.showturtle()
urhajo.pendown() # így ki is rajzolja a röppályát, nem csak az aktuális pozíciót

while urhajo_y >= -0.9*(turtle.window_height()/2):  # -270:
    urhajo.goto(urhajo_x, urhajo_y)

    # első, naiv megközelítés: menjen "45 fokban",
    # azaz 1 pixelt jobbra, 1 pixelt le
    # urhajo_x = urhajo_x + 1
    # urhajo_y = urhajo_y - 1
    urhajo_x = urhajo_x + (urhajo_sebesseg * t * math.cos(math.radians(szog_fok)))
    urhajo_y = urhajo_y + (urhajo_sebesseg * t * math.sin(math.radians(szog_fok)) - (0.5 * g * t**2))
    t = t + 0.1         # ha a mp felbontás túl gyors, akkor legyen 10x lassított felvétel

    # ciklus vége

# ciklus utáni rész, ezt a kintebbi bekezdés jelzi
