# Holdraszállás
# 2.lépés: legyen egy űrhajónk (turtle teknős)
# A változó fogalmának bevezetése.
# Készítette: <írd ide a neved>

# Ide írjuk majd a programunkat.

import turtle
turtle.screensize(800,600)
turtle.bgcolor("grey")

urhajo = turtle.Turtle()
# urhajo.hideturtle()
# urhajo.penup()
urhajo.shape("circle")
urhajo.pen(fillcolor="pink", pencolor="blue")

# Program vége, de némely könyezetben azonnal be is zárná az ablakot.
# Ezért megállítjuk, és a konzol ablakban leütött Enter-re várunk.
# input("Program vége, nyomj Enter-t...")
