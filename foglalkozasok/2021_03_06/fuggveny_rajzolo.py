import turtle
import random
import math

turtle.screensize(800,600)
turtle.bgcolor("grey")

rajzolo = turtle.Turtle()
rajzolo.hideturtle()
rajzolo.penup()
rajzolo.shape("circle")
rajzolo.pen(fillcolor="pink", pencolor="blue")

for x in range(-10, 11): # [int(-1*(turtle.window_width()/2)), int(1*(turtle.window_width()/2))]:
    y = x**2
    rajzolo.goto(x, y)
    rajzolo.pendown()
    # print(x, y)
