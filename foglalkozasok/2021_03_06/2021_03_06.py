>>> import math
>>> mat.pi
Traceback (most recent call last):
  File "<pyshell#27>", line 1, in <module>
    mat.pi
NameError: name 'mat' is not defined
>>> math.pi
3.141592653589793
>>> math.e
2.718281828459045
>>> math.sqrt(16)
4.0
>>> math.sqrt(17)
4.123105625617661
>>> 16**0.5
4.0
>>> 27**(1/3)
3.0
>>> 16**(1/2)
4.0
>>> math.radians(90)
1.5707963267948966
>>> math.radians(90) * 2
3.141592653589793
>>> math.sin(30)
-0.9880316240928618
>>> math.sin(math.radians(30))
0.49999999999999994
>>> math.ceil(4.2)
5
>>> math.ceil(-4.2)
-4
>>> math.floor(4.2)
4
>>> math.floor(-4.2)
-5
>>> math.comb(4,2)
6
>>> math.fabs(4.2)
4.2
>>> math.fabs(-4.2)
4.2
>>> math.fabs(0)
0.0
>>> math.factorial(5)
120
>>> 1*2*3*4*5
120
>>> 7 // 2
3
>>> 7 / 2
3.5
>>> 7 % 2
1
>>> divmod(7, 2)
(3, 1)
>>> math.fmod(7,2)
1.0
>>> math.fsum([1000, 500, 300])
1800.0
>>> int(math.fsum([1000, 500, 300]))
1800
>>> math.gcd(18, 24)
6
>>> math.gcd(18, 24)
6
>>> math.isclose(4.2, 4.2001)
False
>>> math.isclose(4.2, 4.
	     )
False
>>> math.isclose(4.2, 4.200001)
False
>>> math.isclose(4.2, 4.20000001)
False
>>> math.isclose(4.2, 4.200000001)
True
>>> math.isclose(100, 102, rel_tol=5, abs_tol=5)
True
>>> math.isclose(100, 106, rel_tol=5, abs_tol=5)
True
>>> math.isclose(100, 107, rel_tol=5, abs_tol=5)
True
>>> math.isclose(100, 109, rel_tol=5, abs_tol=5)
True
>>> math.isclose(100, 109, rel_tol=1e-09, abs_tol=5)
False
>>> math.isqrt(16)
4
>>> math.sqrt(16)
4.0
>>> math.sqrt(17)
4.123105625617661
>>> math.isqrt(17)
4
>>> math.isqrt(24)
4
>>> math.isqrt(25)
5
>>> math.prod([1,2,3,4,5])
120
>>> math.tau
6.283185307179586
>>> math.inf
inf
>>> math.inf + 1
inf
>>> 
