# 2021-JAN-16: tuple
# TODO konzolból idemásolni

"""
>>> 
>>> 
>>> 
>>> 
>>> lista = ["tej", "kenyér", "tojás"]
>>> lista
['tej', 'kenyér', 'tojás']
>>> lista[0]
'tej'
>>> lista[1]
'kenyér'
>>> lista.append("liszt")
>>> lista
['tej', 'kenyér', 'tojás', 'liszt']
>>> tuple_1 = ("hello", 42)
>>> lista
['tej', 'kenyér', 'tojás', 'liszt']
>>> tuple_1
('hello', 42)
>>> lista[0], tuple_1[0]
('tej', 'hello')
>>> tuple_1[0]
'hello'
>>> tuple_1[1]
42
>>> tuple_1[2]
Traceback (most recent call last):
  File "<pyshell#16>", line 1, in <module>
    tuple_1[2]
IndexError: tuple index out of range
>>> lista[3]
'liszt'
>>> lista[4]
Traceback (most recent call last):
  File "<pyshell#18>", line 1, in <module>
    lista[4]
IndexError: list index out of range
>>> 
>>> 7 / 3
2.3333333333333335
>>> 7 // 3
2
>>> 7 % 3
1
>>> divmod(7, 3)
(2, 1)
>>> divmod(7, 3)[0]
2
>>> divmod(7, 3)[1]
1
>>> (divmod(7, 3))[0]
2
>>> divmod(7, 3)[1]
1
>>> lista
['tej', 'kenyér', 'tojás', 'liszt']
>>> print(lista)
['tej', 'kenyér', 'tojás', 'liszt']
>>> print(enumerate(lista))
<enumerate object at 0x7f6a6ebf6080>
>>> print(list(enumerate(lista)))
[(0, 'tej'), (1, 'kenyér'), (2, 'tojás'), (3, 'liszt')]
>>> furcsa_lista = [0, "tej", 1, "kenyér"]
>>> osztaly_nevsor_szerk = ("Keresztnév", "Vezetéknév", "Cipőméret")
>>> osztaly_nevsor_szerk
('Keresztnév', 'Vezetéknév', 'Cipőméret')
>>> type(osztaly_nevsor_szerk)
<class 'tuple'>
>>> type(lista)
<class 'list'>
>>> osztaly_nevsor = [("Tekla", "Teknős", 38), ("Tóbiás", "Teknős", 37)]
>>> type(osztaly_nevsor)
<class 'list'>
>>> type(osztaly_nevsor[0])
<class 'tuple'>
>>> type(osztaly_nevsor[1])
<class 'tuple'>
>>> osztaly_nevsor
[('Tekla', 'Teknős', 38), ('Tóbiás', 'Teknős', 37)]
>>> osztaly_nevsor[0]
('Tekla', 'Teknős', 38)
>>> osztaly_nevsor[1]
('Tóbiás', 'Teknős', 37)
>>> osztaly_nevsor[0][0]
'Tekla'
>>> osztaly_nevsor[0][2]
38
>>> print(osztaly_nevsor[1][0], osztaly_nevsor[1,2])
Traceback (most recent call last):
  File "<pyshell#46>", line 1, in <module>
    print(osztaly_nevsor[1][0], osztaly_nevsor[1,2])
TypeError: list indices must be integers or slices, not tuple
>>> print(osztaly_nevsor[1][0], osztaly_nevsor[1][2])
Tóbiás 37
>>> egy_elemu_tuple = 42 ,
>>> print(egy_elemu_tuple)
(42,)
>>> print(egy_elemu_tuple), type(egy_elemu_tuple)
(42,)
(None, <class 'tuple'>)
>>> print(egy_elemu_tuple, type(egy_elemu_tuple))
(42,) <class 'tuple'>
>>> valtozo = 42
>>> print(valtozo, type(valtozo))
42 <class 'int'>
>>> valtozo
42
>>> egy_elemu_tuple
(42,)
>>> egy_elemu_tuple[0]
42
>>> print(egy_elemu_tuple[0], type(egy_elemu_tuple[0]))
42 <class 'int'>
>>> talan_tuple = ,
SyntaxError: invalid syntax
>>> ures_tuple = ()
>>> ures_lista = []
>>> print(ures_lista, ures_tuple)
[] ()
>>> print(type(ures_lista), type(ures_tuple))
<class 'list'> <class 'tuple'>
>>> masik_ures_tuple = tuple()
>>> print(masik_ures_tuple, type(masik_ures_tuple))
() <class 'tuple'>
>>> l = pass
SyntaxError: invalid syntax
>>> l = if
SyntaxError: invalid syntax
>>> l = "if"
>>> l, type(l)
('if', <class 'str'>)
>>> tuple_6 = (,,,,,,)
SyntaxError: invalid syntax
>>> tuple_6 = tuple(,,,,,,)
SyntaxError: invalid syntax
>>> tuple_6 = tuple(None,None,None,None,None,None,None)
Traceback (most recent call last):
  File "<pyshell#71>", line 1, in <module>
    tuple_6 = tuple(None,None,None,None,None,None,None)
TypeError: tuple expected at most 1 argument, got 7
>>> tuple_6 = (None,None,None,None,None,None,None)
>>> tuple_6, type(tuple_6)
((None, None, None, None, None, None, None), <class 'tuple'>)
>>> tuple_6[2] = 42
Traceback (most recent call last):
  File "<pyshell#74>", line 1, in <module>
    tuple_6[2] = 42
TypeError: 'tuple' object does not support item assignment
>>> lista_1 = []
>>> print(lista_1)
[]
>>> # szünet
>>> tuple("hello")
('h', 'e', 'l', 'l', 'o')
>>> tuple("hello")[3]
'l'
>>> tuple("hello")[2]
'l'
>>> tuple("hello")[4]
'o'
>>> tuple("hello")[0:2]
('h', 'e')
>>> osztaly_nevsor
[('Tekla', 'Teknős', 38), ('Tóbiás', 'Teknős', 37)]
>>> type(osztaly_nevsor)
<class 'list'>
>>> type(osztaly_nevsor[0])
<class 'tuple'>
>>> keresztneve, csaladneve, cipomerete = osztaly_nevsor[0]
>>> keresztneve, type(keresztneve)
('Tekla', <class 'str'>)
>>> print(keresztneve, type(keresztneve))
Tekla <class 'str'>
>>> print(csaladneve, type(csaladneve))
Teknős <class 'str'>
>>> print(cipomerete, type(cipomerete))
38 <class 'int'>
>>> valtozo = "aaa", "bbb"
>>> keresztneve, csaladneve, cipomerete, hiba = osztaly_nevsor[0]
Traceback (most recent call last):
  File "<pyshell#92>", line 1, in <module>
    keresztneve, csaladneve, cipomerete, hiba = osztaly_nevsor[0]
ValueError: not enough values to unpack (expected 4, got 3)
>>> keresztneve, csaladneve = osztaly_nevsor[0]
Traceback (most recent call last):
  File "<pyshell#93>", line 1, in <module>
    keresztneve, csaladneve = osztaly_nevsor[0]
ValueError: too many values to unpack (expected 2)
>>> v1 = 42
>>> v2 = 37
>>> v1, v2 = v2, v1
>>> v1, v2
(37, 42)
>>> v1
37
>>> v2
42
>>> def f(param):
	pass

>>> f(5)
>>> f("hello")
>>> def f(param):
	print(param, type(param))

	
>>> f(42)
42 <class 'int'>
>>> f("hello")
hello <class 'str'>
>>> f(42,"hello")
Traceback (most recent call last):
  File "<pyshell#109>", line 1, in <module>
    f(42,"hello")
TypeError: f() takes 1 positional argument but 2 were given
>>> f((42,"hello")

  
KeyboardInterrupt
>>> f((42,"hello")

  
KeyboardInterrupt
>>> f((42,"hello"))
(42, 'hello') <class 'tuple'>
>>> p1 = (5,8)
>>> p2 = (4,28)
>>> p1
(5, 8)
>>> p2
(4, 28)
>>> p1 < p2
False
>>> p1 == p2
False
>>> p1 > p2
True
>>> p3 = (5,7)
>>> p1
(5, 8)
>>> p3
(5, 7)
>>> p3 < p1
True
>>> 
>>> 
>>> 
>>> 
>>> cikkek = ["tej", "kenyér", "tojás"]
>>> mennyiseg = [2, 1, 6]
>>> me = ["liter", "kg", "darab"]
>>> cikkek
['tej', 'kenyér', 'tojás']
>>> cikkek[0], mennyiseg[0], me[0]
('tej', 2, 'liter')
>>> zip(cikkek, mennyiseg)
<zip object at 0x7f6a69247f40>
>>> zip(list(cikkek, mennyiseg))
Traceback (most recent call last):
  File "<pyshell#136>", line 1, in <module>
    zip(list(cikkek, mennyiseg))
TypeError: list expected at most 1 argument, got 2
>>> list(zip(cikkek, mennyiseg))
[('tej', 2), ('kenyér', 1), ('tojás', 6)]
>>> list(zip(cikkek, mennyiseg, me))
[('tej', 2, 'liter'), ('kenyér', 1, 'kg'), ('tojás', 6, 'darab')]
>>> cikkek.append("liszt")
>>> list(zip(cikkek, mennyiseg, me))
[('tej', 2, 'liter'), ('kenyér', 1, 'kg'), ('tojás', 6, 'darab')]
>>> cikkek,
(['tej', 'kenyér', 'tojás', 'liszt'],)
>>> cikkek
['tej', 'kenyér', 'tojás', 'liszt']
>>> mennyiseg
[2, 1, 6]
>>> mennyiseg.append(3)
>>> me.append("kg")
>>> list(zip(cikkek, mennyiseg, me))
[('tej', 2, 'liter'), ('kenyér', 1, 'kg'), ('tojás', 6, 'darab'), ('liszt', 3, 'kg')]
>>> 
"""
