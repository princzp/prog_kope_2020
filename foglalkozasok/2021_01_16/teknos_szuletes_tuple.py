# Bolyongás szimuláció teknőcgrafikával.
# A táplálékot kereső állat mozgásának egy lehetséges modellje.
# Bolyongás szimuláció tetszőleges számú teknőssel
# 2021. jan. 16. tuple bevezetése

import turtle
import random

def teknos_szuletik(x, y):
    """Létrehoz egy új teknőst és visszatérési értékként visszaadja.
    A listához nem fűzi hozzá, erről a hívónak kell gondoskodnia.
    Méretét sem állítja be, erről is a hívónak kell gondoskodni.
    Ha x és y koordinátának is nulla értékekkel hívják meg, akkor sorsol az új teknősnek.
    Ellenkező esetben a paraméter x és y koordinátára teszi le.
    """
    uj_teknos = turtle.Turtle()
    uj_teknos.shape("turtle")

    # legyenek sorsoltak a színek
    uj_teknos.pencolor(random.random(), random.random(), random.random())
    uj_teknos.color(random.random(), random.random(), random.random())

    # menjen a helyére (vagy sorsolt, vagy a szülőé)
    uj_teknos.penup()
    
    if x == 0 and y == 0:
        uj_teknos.goto(random.randint(-200, 200), random.randint(-200, 200))
    else:
        uj_teknos.goto(x, y)

    uj_teknos.pendown()

    return uj_teknos

hany_teknos = random.randint(1, 10) # int(input("Hány teknős végezzen bolyongást? "))
print("Kezdődik a szimuláció", hany_teknos, "teknőssel")

# N. B. eddig teknősök egyszerű listája volt.
# Mostantól (teknős, szül. év) rendezett n-esek (n=2), tuple-ok listája
teknos_hadsereg = []

i = 1
while i <= hany_teknos:
    # teknos_hadsereg.append(turtle.Turtle())
    kezdo_teknos = teknos_szuletik(0, 0)

    teknos_tuple = kezdo_teknos, 0
    # teknos_hadsereg.append(kezdo_teknos)
    teknos_hadsereg.append(teknos_tuple)


    i = i + 1

# nyitó ceremónia: üres vászon, sebesség, szín, alak
# turtle.reset()
turtle.Screen()
# turtle.speed(1)

bolyongas_hossza = 1000  # hányszor pörögjön a ciklus
lepes = 1
while lepes <= bolyongas_hossza:

    for teknos, mikor_szuletett in teknos_hadsereg:
        teknos.goto(teknos.xcor() + random.randint(-10, 10), teknos.ycor() + random.randint(-10, 10))

        if lepes % 100 == 0:
            teknos.stamp()

    print(lepes, "/", bolyongas_hossza)  # diagnosztikai print a konzol ablakba

    dobokocka_1 = random.randint(1, 6)
    dobokocka_2 = random.randint(1, 6)
    
    if dobokocka_1 == 6 and dobokocka_2 == 6:
        szulo = random.randint(0, len(teknos_hadsereg)-1)
        # N.B. a teknős hadsereg lista elemei már (teknős, született) tuple-ok
        # szulo_x = teknos_hadsereg[szulo].xcor()
        # szulo_y = teknos_hadsereg[szulo].ycor()

        if lepes - teknos_hadsereg[szulo][1] >= 100:    # tényleg teknős születik
            szulo_x = teknos_hadsereg[szulo][0].xcor()
            szulo_y = teknos_hadsereg[szulo][0].ycor()

            teknos_baba = teknos_szuletik(szulo_x, szulo_y)
            teknos_baba.turtlesize(0.5, 0.5, 0.5)
            # teknos_hadsereg.append(teknos_baba)
            teknos_tuple = teknos_baba, lepes
            teknos_hadsereg.append(teknos_tuple)

            print("Teknős baba született, már", len(teknos_hadsereg), "teknős végez bolyongást.")
        else:   # sajnos túl fiatal, hiába szerencsés
            pass
            print("Sajnos", teknos_hadsereg[szulo][1], "születésű, most nem kelt ki a tojása")
    
    lepes = lepes + 1  # nagyon fontos! (Különben végtelen ciklus)

print("Bolyongás kész.")
