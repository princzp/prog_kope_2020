# 2021.03.27, OOP (2)

# Imperatív példa lett volna
def altalanos_kerulet(sikidom):
    pass
    # if háromszög
    # elif kör
    # ...

import math

class SikIdom():

    def kerulet(self):
        print("Síkidom kerülete")
        return 0

    def terulet(self):
        print("Síkidom területe")
        return 0

    def nagyobb_mint(self, other):
        if self.terulet() > other.terulet():
            return True
        else:
            return False

class Kor(SikIdom):
    # Körnek van 1 paramétere, a sugara

    def __init__(self, kor_sugara):
        self.r = kor_sugara

    def kerulet(self):
        kerulet = 2 * self.r * math.pi
        print("Ennek a körnek", self.r, "a sugara, és", kerulet, "a kerülete")
        return kerulet

    def terulet(self):
        terulet = (self.r ** 2) * math.pi
        print("Ennek a körnek", self.r, "a sugara, és", terulet, "a területe")
        return terulet

class Haromszog(SikIdom):

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def kerulet(self):
        # print("Háromszög kerülete")
        return self.a + self.b + self.c

    def terulet(self):
        # Heron-képlet
        # s = (self.a + self.b + self.c) / 2

        s = self.kerulet() / 2
        terulet = math.sqrt(s * (s-self.a) * (s-self.b) * (s-self.c))
        return terulet

class Teglalap(SikIdom):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def kerulet(self):
        return 2*(self.a + self.b)

    def terulet(self):
        return self.a * self.b

    def tulajdonsag(self):
        print("Téglalap: 2-2 szemközti oldala egyenlő, 4 szöge egyenlő.", self)

class Rombusz(SikIdom):
    def __init__(self, a):
        self.a = a

    def tulajdonsag(self):
        print("Rombusz: 2-2 szemközti szöge egyenlő, 4 oldala egyenlő.", self)

class Negyzet(Teglalap, Rombusz):
    def __init__(self, a):
        # Teglalap.__init__(a, a)
        super().__init__(a, a)

        Teglalap.tulajdonsag(self)
        Rombusz.tulajdonsag(self)

        print("Négyzet: téglalap és rombusz egyszerre: 4 oldala és 4 szöge is egyenlő.", self)
