Python 3.8.5 (default, Jul 20 2020, 19:48:14) 
[GCC 7.5.0] on linux
Type "help", "copyright", "credits" or "license()" for more information.
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_20/objektum_orientaltsag.py
>>> s1=SikIdom()
>>> s2=SikIdom()
>>> k1=Kor()
Traceback (most recent call last):
  File "<pyshell#2>", line 1, in <module>
    k1=Kor()
TypeError: __init__() missing 1 required positional argument: 'kor_sugara'
>>> k1=Kor(3)
>>> s1.kerulet()
Síkidom kerülete
>>> k1.kerulet()
Ennek a körnek 3 a sugara, és 18.84955592153876 a kerülete
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> k1=Kor(3)
>>> k2=Kor(5)
>>> 
>>> k1.kerulet()
Ennek a körnek 3 a sugara, és 18.84955592153876 a kerülete
18.84955592153876
>>> k1.terulet()
Ennek a körnek 3 a sugara, és 28.274333882308138 a területe
>>> k2.kerulet()
Ennek a körnek 5 a sugara, és 31.41592653589793 a kerülete
31.41592653589793
>>> k2.terulet()
Ennek a körnek 5 a sugara, és 78.53981633974483 a területe
>>> k1.nagyobb_mint(k1,k2)
Traceback (most recent call last):
  File "<pyshell#13>", line 1, in <module>
    k1.nagyobb_mint(k1,k2)
TypeError: nagyobb_mint() takes 2 positional arguments but 3 were given
>>> k1.nagyobb_mint(k2)
Ennek a körnek 3 a sugara, és 28.274333882308138 a területe
Ennek a körnek 5 a sugara, és 78.53981633974483 a területe
Traceback (most recent call last):
  File "<pyshell#14>", line 1, in <module>
    k1.nagyobb_mint(k2)
  File "/home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py", line 23, in nagyobb_mint
    if self.terulet() > other.terulet():
TypeError: '>' not supported between instances of 'NoneType' and 'NoneType'
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> k1=Kor(3)
>>> k2=Kor(5)
>>> k1.nagyobb_mint(k2)
Ennek a körnek 3 a sugara, és 28.274333882308138 a területe
Ennek a körnek 5 a sugara, és 78.53981633974483 a területe
False
>>> k2.nagyobb_mint(k1)
Ennek a körnek 5 a sugara, és 78.53981633974483 a területe
Ennek a körnek 3 a sugara, és 28.274333882308138 a területe
True
>>> k2.nagyobb_mint(k2)
Ennek a körnek 5 a sugara, és 78.53981633974483 a területe
Ennek a körnek 5 a sugara, és 78.53981633974483 a területe
False
>>> k1.nagyobb_mint(k1)
Ennek a körnek 3 a sugara, és 28.274333882308138 a területe
Ennek a körnek 3 a sugara, és 28.274333882308138 a területe
False
>>> s1=SikIdom()
>>> s1.kerulet()
Síkidom kerülete
0
>>> s1.terulet()
Síkidom területe
0
>>> k1.nagyobb_mint(s1)
Ennek a körnek 3 a sugara, és 28.274333882308138 a területe
Síkidom területe
True
>>> s1.nagyobb_mint(k1)
Síkidom területe
Ennek a körnek 3 a sugara, és 28.274333882308138 a területe
False
>>> h1=Haromszog()
>>> type(h1)
<class '__main__.Haromszog'>
>>> h1.kerulet()
Háromszög kerülete
>>> t1=Teglalap()
>>> type(t1)
<class '__main__.Teglalap'>
>>> t1.kerulet()
Síkidom kerülete
0
>>> Háromszög kerülete
SyntaxError: invalid syntax
>>> 
>>> 
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> h1=Haromszog(3,4,5)
>>> k1=Kor(2)
>>> h1.kerulet()
12
>>> h1.terulet()
6.0
>>> k1.kerulet()
Ennek a körnek 2 a sugara, és 12.566370614359172 a kerülete
12.566370614359172
>>> k1.terulet()
Ennek a körnek 2 a sugara, és 12.566370614359172 a területe
12.566370614359172
>>> k1.nagyobb_mint(h1)
Ennek a körnek 2 a sugara, és 12.566370614359172 a területe
True
>>> h1.nagyobb_mint(k1)
Ennek a körnek 2 a sugara, és 12.566370614359172 a területe
False
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> t1=Teglalap(3,4)
>>> t1.kerulet()
Traceback (most recent call last):
  File "<pyshell#44>", line 1, in <module>
    t1.kerulet()
  File "/home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py", line 69, in kerulet
    return 2*(a+b)
NameError: name 'a' is not defined
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> t1=Teglalap(3,4)
>>> t1.kerulet()
14
>>> t1.terulet()
12
>>> h1=Haromszog(3,4,5)
>>> h1.terulet()
6.0
>>> t1.nagyobb_mint(h1)
True
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> t1=Teglalap(3,4)
>>> r1=Rombusz(5)
>>> r1.kerulet()
Síkidom kerülete
0
>>> t1.tulajdonsag()
Téglalap: 2-2 szemközti oldala egyenlő, 4 szöge egyenlő.
>>> r1.tulajdonsag()
Rombusz: 2-2 szemközti szöge egyenlő, 4 oldala egyenlő.
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> n1=Negyzet(5)
Traceback (most recent call last):
  File "<pyshell#56>", line 1, in <module>
    n1=Negyzet(5)
  File "/home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py", line 88, in __init__
    Teglalap.tulajdonsag()
TypeError: tulajdonsag() missing 1 required positional argument: 'self'
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> n1=Negyzet(5)
Téglalap: 2-2 szemközti oldala egyenlő, 4 szöge egyenlő. <__main__.Negyzet object at 0x7f2cf2178730>
Rombusz: 2-2 szemközti szöge egyenlő, 4 oldala egyenlő. <__main__.Negyzet object at 0x7f2cf2178730>
Négyzet: téglalap és rombusz egyszerre: 4 oldala és 4 szöge is egyenlő. <__main__.Negyzet object at 0x7f2cf2178730>
>>> n1=Negyzet(4)
Téglalap: 2-2 szemközti oldala egyenlő, 4 szöge egyenlő. <__main__.Negyzet object at 0x7f2cf2178b50>
Rombusz: 2-2 szemközti szöge egyenlő, 4 oldala egyenlő. <__main__.Negyzet object at 0x7f2cf2178b50>
Négyzet: téglalap és rombusz egyszerre: 4 oldala és 4 szöge is egyenlő. <__main__.Negyzet object at 0x7f2cf2178b50>
>>> t1=Teglalap(10,12)
>>> t1.tulajdonsag()
Téglalap: 2-2 szemközti oldala egyenlő, 4 szöge egyenlő. <__main__.Teglalap object at 0x7f2cf2178c40>
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> n1=Negyzet(4)
Traceback (most recent call last):
  File "<pyshell#61>", line 1, in <module>
    n1=Negyzet(4)
  File "/home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py", line 86, in __init__
    super().__init__(a, a)
TypeError: __init__() takes 2 positional arguments but 3 were given
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> n1=Negyzet(4)
Téglalap: 2-2 szemközti oldala egyenlő, 4 szöge egyenlő. <__main__.Negyzet object at 0x7fdca06a6730>
Rombusz: 2-2 szemközti szöge egyenlő, 4 oldala egyenlő. <__main__.Negyzet object at 0x7fdca06a6730>
Négyzet: téglalap és rombusz egyszerre: 4 oldala és 4 szöge is egyenlő. <__main__.Negyzet object at 0x7fdca06a6730>
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> h1=Haromszog(3,4,5)
>>> k1=Kor(5)
>>> t1=Teglalap(4,5)
>>> r1=Rombusz(6)
>>> n1=Negyzet(8)
Téglalap: 2-2 szemközti oldala egyenlő, 4 szöge egyenlő. <__main__.Negyzet object at 0x7fa8362fcbe0>
Rombusz: 2-2 szemközti szöge egyenlő, 4 oldala egyenlő. <__main__.Negyzet object at 0x7fa8362fcbe0>
Négyzet: téglalap és rombusz egyszerre: 4 oldala és 4 szöge is egyenlő. <__main__.Negyzet object at 0x7fa8362fcbe0>
>>> isinstance(k1,Kor)
True
>>> isinstance(k1,Haromszog)
False
>>> isinstance(k1,SikIdom)
True
>>> s1=SikIdom()
>>> isinstance(n1,Negyzet)
True
>>> isinstance(n1,Haromszog)
False
>>> isinstance(n1,Teglalap)
True
>>> isinstance(n1,Rombusz)
True
>>> issubclass(Haromszog,SikIdom)
True
>>> issubclass(Negyzet,Teglalap)
True
>>> issubclass(Negyzet,Rombusz)
True
>>> issubclass(Negyzet,Haromszog)
False
>>> issubclass(Negyzet,Negyzet)
True
>>> issubclass(Negyzet,SikIdom)
True
>>> issubclass(SikIdom,Negyzet)
False
>>> SikIdom.__subclasses__()
[<class '__main__.Kor'>, <class '__main__.Haromszog'>, <class '__main__.Teglalap'>, <class '__main__.Rombusz'>]
>>> Kor.__subclasses__()
[]
>>> Teglalap.__subclasses__()
[<class '__main__.Negyzet'>]
>>> Rombusz.__subclasses__()
[<class '__main__.Negyzet'>]
>>> Negyzet.mro()
[<class '__main__.Negyzet'>, <class '__main__.Teglalap'>, <class '__main__.Rombusz'>, <class '__main__.SikIdom'>, <class 'object'>]
>>> n1.nagyobb_mint(Kor(5))
Ennek a körnek 5 a sugara, és 78.53981633974483 a területe
False
>>> n1.kerulet()
32
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> n1=Negyzet(8)
Traceback (most recent call last):
  File "<pyshell#90>", line 1, in <module>
    n1=Negyzet(8)
  File "/home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py", line 86, in __init__
    Teglalap.__init__(a, a)
TypeError: __init__() missing 1 required positional argument: 'b'
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> n1=Negyzet(8)
Traceback (most recent call last):
  File "<pyshell#91>", line 1, in <module>
    n1=Negyzet(8)
  File "/home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py", line 86, in __init__
    Teglalap.__init__(a, a)
TypeError: __init__() missing 1 required positional argument: 'b'
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> n1=Negyzet(8)
Traceback (most recent call last):
  File "<pyshell#92>", line 1, in <module>
    n1=Negyzet(8)
  File "/home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py", line 87, in __init__
    super().__init__(a, a)
TypeError: __init__() takes 2 positional arguments but 3 were given
>>> 
= RESTART: /home/ethppz/projects/elte/PhD/kope_szakkor_2020_2021/public/foglalkozasok/2021_03_27/objektum_orientaltsag.py
>>> n1=Negyzet(8)
Téglalap: 2-2 szemközti oldala egyenlő, 4 szöge egyenlő. <__main__.Negyzet object at 0x7f97e7c82730>
Rombusz: 2-2 szemközti szöge egyenlő, 4 oldala egyenlő. <__main__.Negyzet object at 0x7f97e7c82730>
Négyzet: téglalap és rombusz egyszerre: 4 oldala és 4 szöge is egyenlő. <__main__.Negyzet object at 0x7f97e7c82730>
>>> Negyzet.mro()
[<class '__main__.Negyzet'>, <class '__main__.Teglalap'>, <class '__main__.Rombusz'>, <class '__main__.SikIdom'>, <class 'object'>]
>>> 