## JÖN (Tervezett, laza tematika, nem feltétlenül ebben a sorrendben)
---
* Műveletek, függvények listákon: lista hossza, string tördelése szavak listájára, szavak listájának összefűzése stringbe. Lista helyben rendezése.
* Lista és változó hivatkozások, értékadások közötti különbség.
* Gyakorlat: cenzor() függvény megírása: stringből adott mintának megfelelő szót kisípolni.
---
* Közös programozás: számkitalálós játék, szorzótábla gyakoroltató játék
---
* Kitekintés: programozás érettségi, jó szívvel ajánlható fejlesztő környezetek
	* Programozás érettségi 2020
		* Használható ingyenes szoftver: https://www.oktatas.hu/kozneveles/erettsegi/2020tavaszi_vizsgaidoszak/informatika_ingyenes_szoftverek_2020
		* Emelt szint feladatsorok: https://www.oktatas.hu/kozneveles/erettsegi/feladatsorok/emelt_szint_2020osz/emelt_8nap
	* Nevezetes Python fejlesztő környezetek (érettségin OK)
		* IDLE
		* Geany
		* esetleg Eclipse?
	* Nevezetes Python fejlesztő környezetek (a fentieken felül, tanulni ajánlhatóak)
		* thonny
		* PyCharm
		* VS Code
		* Jupyter Notebook
		* pythontutor.com
---
* Gondolkodtató feladat elágazásokkal kapcsolatban: FizzBuzz (RókaBéka) oszthatósági játék.
* Gondolkodtató feladat ciklusokkal kapcsolatban: hátultesztelő ciklus megvalósítása Python-ban (vagy más nyelven)
* Tipikus játékprogram hurok: látszólag végtelen ciklus egy feltételes break-kel, ami a játék végét kezeli...
---
* Kör, ellipszis és tojás rajzolása teknőcgrafikával. (Módszer: [x-r,x+r] intervallumon Pitagoasz-tétellel kettesével rajzolni a pontokat.)
* Holdraszállós játék: ferde hajítás szimuláció, másodfokú egyenlet, parabola (NB: NAT 9. évfolyam, 4 óra! 2 szakköri alkalom?) <br>
https://gitlab.com/princzp/4hr_python_workshop
* Gombahatározós gépi tanuló algoritmus (naïve Bayes osztályozás, NAT 12+ val. szám!)
---
* Rekurzió.
	* Rekurzió programban és adatszerkezetben: rekurzív függvény, rekurzív adatszerkezet. 
	* Fibonacci-számok, Fibonacci-sorozat kiszámítása.
	* Listák és fák, mint rekurzív adatszerkezetek.
---
* String bővebben
    * két string konkatenációja
    * string ismétlése (szorzata)
    * string hossza
    * indexelés
    * kivágás, szeletelés
    * string bejárása for .. in iterátorral
    * tartalmazás vizsgálata: in, not in
    * összehasonlítás
    * változó behelyettesítése string-be
    * stringek típuskényszerítése: int(), float()
    * string metódusok (rengeteg, ld. [Tut] 4.7.1)
* Lista bővebben
    * elemek elérése, indexelés
    * lista módosítása
    * szeletelés
    * számok listája range()-el
    * lista hossza
    * lista bejárása for .. in iterátorral
    * elemek lista végére fűzése
    * tartalmazás vizsgálata: in, not in
    * lista másolása
    * lista metódusok (rengeteg, ld. [Tut] 5.1)
    * többdimenziós listák: listák egymásba ágyazása
---
* Kivételkezelés. Futás közbeni hibák.
* Számkitalálós játék kiegészítése kivételkezeléssel.
* Közös programozás: torpedó játék.
* Filekezelés, I/O.
* Adatok mentése fileba, visszatöltése fileból. JSON. Típuskonverzió.
---
* Hasznos modulok, standard library túra, "battery included" filozófia
	* Parancs-sori paraméterek. A sys modul.
	* Web lekérdezés pythonból. Az urllib modul.
	* Dátum és idő manipuláció. A datetime modul.
	* Tesztelés. A doctest modul.
	* Egyéb hasznos modulok.
---
* Objektum-orientált programozási paradigma
	* Osztály fogalma és objektum fogalma. Példányosítás. A class és self kulcsszavak.
	* Leszármaztatás, öröklődés. Szülő és gyermek osztályok.
	* Objektumok inicializálása: az \_\_init\_\_ függvény
	* Példányváltozók. Attribútumok, property-k, getter/setter függvények.
	* Egységbe zárás.
	* Objektumok kompozíciója.
	* Polimorfizmus. Duck typing.
	* Absztrakt osztály és metódus
---
* Szakköri összefoglaló:
    * Miről volt szó?
    * Mi maradt ki?
    * Merre érdemes továbbmenni?
* Kérdezz-felelek.
---
* ... Stb., ami még alakul ...
---
* Legvégén: import this

